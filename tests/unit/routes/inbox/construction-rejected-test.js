import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | inbox/construction-rejected', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:inbox/construction-rejected');
    assert.ok(route);
  });
});
