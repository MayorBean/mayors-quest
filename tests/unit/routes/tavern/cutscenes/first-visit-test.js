import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | tavern/cutscenes/first-visit', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:tavern/cutscenes/first-visit');
    assert.ok(route);
  });
});
