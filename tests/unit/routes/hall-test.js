import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | hall', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:hall');
    assert.ok(route);
  });
});
