import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | settlement/heroes', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:settlement/heroes');
    assert.ok(route);
  });
});
