import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | barracks', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:barracks');
    assert.ok(route);
  });
});
