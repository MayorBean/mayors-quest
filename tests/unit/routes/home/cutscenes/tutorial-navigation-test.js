import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | home/cutscenes/tutorial-navigation', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:home/cutscenes/tutorial-navigation');
    assert.ok(route);
  });
});
