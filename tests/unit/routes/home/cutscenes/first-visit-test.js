import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | home/cutscenes/first-visit', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:home/cutscenes/first-visit');
    assert.ok(route);
  });
});
