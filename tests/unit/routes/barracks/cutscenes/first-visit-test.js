import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | barracks/cutscenes/first-visit', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:barracks/cutscenes/first-visit');
    assert.ok(route);
  });
});
