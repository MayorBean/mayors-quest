import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | hall/reception', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:hall/reception');
    assert.ok(route);
  });
});
