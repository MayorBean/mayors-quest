import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | hall/registry', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:hall/registry');
    assert.ok(route);
  });
});
