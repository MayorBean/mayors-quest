const Bcrypt = require("bcrypt");
const config = require('../config.js');
const jwt = require('jsonwebtoken');
const Player = require("../models/player");

//Create new player
exports.create = (req, res) => {
	Player.findOne({ username: req.body.username }, function(err, player){
		if (!player) {
			return res.status(400).send({ message: "Invalid username or password." });
		}
		if (Bcrypt.compareSync(req.body.password, player.password)) {
			// Return player ID and username with authorization token header
			console.log(player.username + " is logging in");
			const token = jwt.sign({_id: player._id}, config.privatekey);
			res.header("x-auth-token", token).status(200).send({
				_id: player._id,
				username: player.username,
				worlds: player.worlds,
				access_token: token
			});
		} else {
			return res.status(400).send({ message: "Invalid username or password." });
		}
	})
};
