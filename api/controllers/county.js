const County = require('../models/county.js');
const Settlement = require('../models/town.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

async function generate(world, regions) {
	let counties = [];
	for (let i=0; i<regions.length; i++) {
		// In future fetch number of counties from settings.size
		for (let j=0; j<(world.settings.size + 3); j++) {
			biomeName = dice.random(regions[i].climate.biomes);
			let biome = world.settings.biomes.filter(biome => biome.name === biomeName)[0]
			let county = new County({
				name: name.county(world.settings.names),
				region: regions[i],
				world: regions[i].world,
				biome: biome
			});
			counties.push(county);
		}
	}
	let promises = counties.map((county) => {return county.save()});
	return await Promise.all(promises);
}

exports.generate = generate;

async function finalise(counties) {
	for (let i = 0; i < counties.length; i++) {
		let settlements = await Settlement.find({county: counties[i]});
		let sortedSettlements = dice.sort('reputation', settlements);
		if (!sortedSettlements[0]) {console.log(settlements)} // occasionally this isn't set for some reason
		let capital = sortedSettlements[0]._id;
		counties[i].set('capital', capital);
		counties[i].save();
	}
}

exports.finalise = finalise;

// Create new county
exports.create = (req, res) => {

	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "county settings cannot be empty"
		});
	}

	// Create county
	console.log("Received POST request for new county: " + JSON.stringify(req.body.county, null, 2));
	let county = req.body.county;
	const newcounty= new County({
		name:  county.name,
		region:  county.region || null,
		capital: county.capital,
		world: county.world,
		biome: county.biome,
		towns: county.towns || []
	});

	// Save county in database
	newcounty.save().then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save county."
		});
	});
};

// Retrieve all counties from the database
exports.findAll = (req, res) => {
	console.log("received GET request for counties: " + JSON.stringify(req.query, null, 2));
	if (req.query) {
		County.find(req.query)
		.then(counties => {
			res.send(counties);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to find any counties with query: " + req.query
			});
		});
	} else {
		County.find()
		.then(counties => {
			res.send(counties);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to list counties."
			});
		});
	}
};

// Find a single county by id
exports.findOne = (req, res) => {
	console.log("received GET request for county: " + req.params.county_id);
	County.findById(req.params.county_id)
	.then(county => {
		if(!county) {
			return res.status(404).send({
				message: "county not found with id " + req.params.county_id
			});
		}
		res.send(county);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "county not found with id " + req.params.county_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve county with id " + req.parms.county_id
		});
	});
};

// Update a county
exports.update = (req, res) => {
	console.log("Received PUT request for county:" + JSON.stringify(req.body, null, 2));
	//Validate request
	if(!req.body) {
		console.log("county content cannot be empty");
		return res.status(400).send({
			message: "county content cannot be empty"
		});
	}

	// Find and update county with req.params
	console.log("county: " + req.params.county_id + " is being set to " + JSON.stringify(req.body, null, 2));
	let county = req.body.county;
	County.findByIdAndUpdate(req.params.county_id, {
		name:  county.name,
		region:  county.region,
		capital: county.capital,
		world: county.world,
		biome: county.biome,
		towns: county.towns
	}, {new: true})
	.then(county => {
		if(!county) {
			console.log("county not found with id: "  + req.params.county_id);
			return res.status(404).send({
				message: "county not found with id: " + req.params.county_id
			});
		}
		res.send(county);
	}).catch(err => {
		console.log("There was an error saving the county.");
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "county not found with id " + req.params.county_id
			});
		}
		return res.status(500).send({
			message: "Unable to update county with id " + req.parms.county_id
		});
	});
};

// Delete a county by id
exports.delete = (req, res) => {
	County.findByIdAndRemove(req.params.county_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted county with id: ' + req.params.county_id);
		res.send('Successfully deleted county with id: ' + req.params.county_id);
	})
};
