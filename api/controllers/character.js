const Character = require('../models/character.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

function getFirstName(gender, names) {
	if (gender === 'female') {
		return name.firstName(names.female)
	} else {
		return name.firstName(names.male)
	}
}

function getPhysicalAttributes() {
	return {
		agility:      dice.normal(10, 1, 20),
		balance:      dice.normal(10, 1, 20),
		dexterity:    dice.normal(10, 1, 20),
		reflexes:     dice.normal(10, 1, 20),
		speed:        dice.normal(10, 1, 20),
		stamina:      dice.normal(10, 1, 20),
		strength:     dice.normal(10, 1, 20)
	};
}

function getMentalAttributes() {
	return {
		bravery:      dice.normal(10, 1, 20),
		charisma:     dice.normal(10, 1, 20),
		composure:    dice.normal(10, 1, 20),
		creativity:   dice.normal(10, 1, 20),
		intelligence: dice.normal(10, 1, 20),
		leadership:   dice.normal(10, 1, 20),
		wisdom:       dice.normal(10, 1, 20)
	};
}

function getHiddenAttributes() {
	return {
		adaptability: dice.normal(10, 1, 20),
		ambition:     dice.normal(10, 1, 20),
		chivalry:     dice.normal(10, 1, 20),
		discipline:   dice.normal(10, 1, 20),
		honor:        dice.normal(10, 1, 20),
		loyalty:      dice.normal(10, 1, 20),
		versatility:  dice.normal(10, 1, 20)
	};
}

function getDateOfBirth(min, max){
	return {
		hour: dice.roll(4),
		day: dice.roll(28),
		season: dice.roll(4),
		year: (752 - (dice.roll(max - min) + min))
	}
}

function getProfession(world, buildingType) {
	let buildings = world.settings.buildings.filter(building => building.formType === buildingType);
	return buildings.length ? dice.random(buildings).profession : dice.random(world.settings.professions);
}

function getAbility(date, potential, dateOfBirth, hiddenAttributes) {
	if (!dateOfBirth) {let dateOfBirth = getDateOfBirth(0, 100)}
	let age = calculateAge(date, dateOfBirth);
	let peakStart = 30 + (hiddenAttributes.ambition - 10);
	let peakEnd = 40 + (hiddenAttributes.discipline - 10);
	let ability;
	if (age < peakStart) {ability = (age / peakStart) * potential}
	if (age > peakEnd) {ability = (peakEnd / age) * potential}
	if (age >= peakStart && age <= peakEnd) {ability = potential}
	return Math.round(ability);
}

function calculateAge(currentDate, dateOfBirth) {
	if (dateOfBirth.season < currentDate.season) {return currentDate.year - dateOfBirth.year}
	if (dateOfBirth.season > currentDate.season) {return currentDate.year - dateOfBirth.year - 1}
	if (dateOfBirth.season == currentDate.season && dateOfBirth.day < currentDate.day) {return currentDate.year - dateOfBirth.year}
	if (dateOfBirth.season == currentDate.season && dateOfBirth.day > currentDate.day) {return currentDate.year - dateOfBirth.year - 1}
	if (dateOfBirth.season == currentDate.season && dateOfBirth.day == currentDate.day) {return currentDate.year - dateOfBirth.year}
}

function getLevel(dateOfBirth, reputation, hiddenAttributes, date) {
	let peak = 40 + ((hiddenAttributes.ambition - 10))
	let age = calculateAge(date, dateOfBirth);
	if (age > peak) {return Math.ceil(reputation / 20)}
	else {return Math.ceil((reputation / 20) / (peak / age))};
}

function generate(world, settlements, params) {
	let gender = dice.random(["male", "female"]);
	let firstName = getFirstName(gender, world.settings.names);
	if (!params.potential) {params.potential = dice.normal(params.reputation, 1, 200)};
	if (!params.hiddenAttributes) {params.hiddenAttributes = getHiddenAttributes()}
	if (!params.dateOfBirth) {params.dateOfBirth = getDateOfBirth(1, 100)}
	let ability = getAbility(world.date, params.potential, params.dateOfBirth, params.hiddenAttributes);
	return new Character({
		firstName:          params.firstName          || firstName,
		lastName:           params.lastName           || name.town(world.settings.names),
		gender:             params.gender             || gender,
		race:               params.race               || "human",
		world:              params.world              || world,
		level:              params.level              || getLevel(params.dateOfBirth, params.reputation, params.hiddenAttributes, world.date),
		realEstate:         params.realEstate         || null,
		birthplace:         params.birthplace         || dice.random(settlements),
		home:               params.home               || null,
		location:           params.location           || null,
		profession:         params.profession         || dice.random(world.settings.professions),
		class:              params.class              || null,
		trait:              params.trait              || null,
		dateOfBirth:        params.dateOfBirth,
		experience:         params.experience         || 0,
		reputation:         params.reputation         || dice.normal(100, 1, 200),
		morale:             params.morale             || "okay",
		status:             params.status             || "none",
		physicalAttributes: params.physicalAttributes || getPhysicalAttributes(),
		mentalAttributes:   params.mentalAttributes   || getMentalAttributes(),
		hiddenAttributes:   params.hiddenAttributes,
		potential:          params.potential,
		ability:            params.ability            || ability,
		inventory:          params.inventory          || [],
		deformities:        params.deformities        || null,
		playersMet:         params.playersMet         || []
	});
}

async function populate(world, settlements, buildings) {
	let characters = [];
	for (let i = 0; i < settlements.length; i++) {
		let mayor = generate(world, settlements, {
			profession: 'mayor',
			home: settlements[i],
			dateOfBirth: getDateOfBirth(25, 75),
			reputation: dice.normal(settlements[i].reputation, 1, 200),
		});
		characters.push(mayor);
		// Spawn 2-12 heroes per town
		for (let j = 0; j < Math.floor(settlements[i].reputation / 20) + 2; j++) {
			let reputation = dice.normal(settlements[i].reputation, 1, 200);
			let level = Math.ceil(reputation / 20);
			let hero = generate(world, settlements, {
				profession: 'hero',
				class: dice.random(world.settings.classes),
				home: settlements[i],
				dateOfBirth: getDateOfBirth(12, 50),
				reputation: reputation,
			});
			characters.push(hero);
		}
	}
	buildings.forEach((building) => {
		if (building.type == 'tavern') {
			for (let i = 0; i < 4; i++) {
				let reputation = dice.normal(building.reputation, 1, 200);
				let level = Math.ceil(reputation / 20);
				let adventurer = generate(world, settlements, {
					profession: 'adventurer',
					class: dice.random(world.settings.classes),
					location: building.get('town'),
					dateOfBirth: getDateOfBirth(12, 50),
					reputation: reputation,
				});
				characters.push(adventurer);
			}
		}
		let owner = generate(world, settlements, {
			profession: getProfession(world, building.type),
			home: building.town,
			location: building.get('town'),
			dateOfBirth: getDateOfBirth(18, 80),
			reputation: dice.normal(building.reputation, 1, 200),
			realEstate: building
		});
		characters.push(owner);
	});
	let promises = characters.map((character) => {
		character.save();
	});
	return await Promise.all(promises);
}

exports.populate = populate;
exports.generate = generate;
exports.getDateOfBirth = getDateOfBirth;

//Create new character
exports.create = (req, res) => {
	console.log("Received POST request for new character" + JSON.stringify(req.body, null, 2));

	//Request validation
	if(!req.body) {
		console.log("Character settings cannot be empty");
		return res.status(400).send({
			message: "Character settings cannot be empty"
		});
	}

	// Create character
	const post = req.body.character;
	const character = new Character({
		world:							post.world,
		firstName:    	  	post.firstName,
		lastName:     	  	post.lastName,
		gender:       	  	post.gender,
		race:						 		post.race,
		world:							post.world,
		birthplace:      		post.birthplace,
		home:            		post.home,
		location:        	  post.location,
		profession:       	post.profession,
		class: 							post.class,
		trait:            	post.trait,
		dateOfBirth:        post.dateOfBirth,
		experience:    	  	post.experience,
		reputation:    	  	post.reputation,
		morale: 						post.morale,
		status: 						post.status,
		mentalAttributes: 	post.mentalAttributes,
		physicalAttributes: post.physicalAttributes,
		hiddenAttributes: 	post.hiddenAttributes,
		potential:					post.potential,
		ability:						post.ability,
		deformities:				post.deformities,
		realEstate:					post.realEstate,
		level: 							post.level
	});

	//Save character in database
	character.save()
	.then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save character."
		});
	});
};

//Retrieve all characters from the database
exports.findAll = (req, res) => {
	console.log("received GET request for characters")
	if (req.query) {
		Character.find(req.query)
		.then(characters => {
			res.send(characters);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to find any characters matching query: " + req.query
			});
		});
	} else {
		Character.find()
		.then(characters => {
			res.send(characters);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to list characters."
			});
		});
	}
};

// Find a single character by id
exports.findOne = (req, res) => {
	console.log("received GET request for character: " + req.params.character_id);
	Character.findById(req.params.character_id)
	.then(character => {
		if(!character) {
			return res.status(404).send({
				message: "character not found with id " + req.params.character_id
			});
		}
		res.send(character);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "character not found with id " + req.params.character_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve character with id " + req.parms.character_id
		});
	});
};

// Update a character
exports.update = (req, res) => {
	console.log("Received PUT request for character:" + JSON.stringify(req.body, null, 2));
	//Validate request
	if(!req.body) {
		console.log("character content cannot be empty");
		return res.status(400).send({
			message: "character content cannot be empty"
		});
	}

	// Find and update character with req.body
	console.log("character id: " + req.params.character_id) + "is being set to " + JSON.stringify(req.body, null, 2);
	const put = req.body.character;
	Character.findByIdAndUpdate(req.params.character_id, {
		world:							put.world,
		firstName:    	   	put.firstName,
		lastName:     	   	put.lastName,
		gender:       	   	put.gender,
		race:						 	 	put.race,
		world:						 	put.world,
		birthplace:      	 	put.birthplace,
		home:            	 	put.home,
		location:        	 	put.location,
		profession:        	put.profession,
		class: 						 	put.class,
		trait:             	put.trait,
		dateOfBirth:        put.dateOfBirth,
		experience:    	   	put.experience,
		reputation:    	   	put.reputation,
		morale: 					 	put.morale,
		status: 					 	put.status,
		physicalAttributes: put.physicalAttributes,
		mentalAttributes: 	put.mentalAttributes,
		hiddenAttributes: 	put.hiddenAttributes,
		potential:					put.potential,
		ability:					  put.ability,
		deformities:				put.deformities,
		realEstate:					put.realEstate,
		level: 							put.level
	}, {new: true})
	.then(character => {
		if(!character) {
			console.log("character object not found.");
			return res.status(404).send({
				message: "character not found with id " + req.params.character_id
			});
		}
		res.send(character);
	}).catch(err => {
		console.log("There was an error saving the character.");
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "character not found with id " + req.params.character_id
			});
		}
		return res.status(500).send({
			message: "Unable to update character with id " + req.parms.character_id
		});
	});
};

// Delete a character by id
exports.delete = (req, res) => {
	Character.findByIdAndRemove(req.params.character_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted character with id: ' + req.params.character_id);
		res.send({message: 'Successfully deleted character with id: ' + req.params.character_id});
	});
};
