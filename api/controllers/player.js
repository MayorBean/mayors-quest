const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");
const config = require('../config.js');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const Player = require("../models/player");

//Create new player
exports.create = (req, res) => {

	// validate the request body first
	const schema = {
		username: Joi.string().min(3).max(50).required(),
		password: Joi.string().min(3).max(60).required()
	};
  const { error } = Joi.validate(req.body.player, schema);
  if (error) return res.status(400).send(error.details[0].message);

	player = new Player({
		username: req.body.player.username,
		password: req.body.player.password
	})
	player.password = bcrypt.hashSync(player.password, 10);
	player.save().then(result => {
		const token = jwt.sign({_id: this._id}, config.privatekey);
		res.header("x-auth-token", token).status(200).send({
			_id: player._id,
			username: player.username
		});
	}).catch(err => {
		console.log(err.message);
		return res.status(400).send(err.message);
	})
};

exports.findAll = (req, res) => {
	console.log("received GET request for players");
	Player.find()
	.then(players => {
		res.send(players);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to list players."
		});
	});
};

exports.findOne = (req, res) => {
	console.log("looking for player: " + req.body.player.username);
	const player = Player.findOne(req.body.player.username).select("-password");
	res.send(player);
};

// Find a single player by id
exports.findById = (req, res) => {
	console.log("received GET request for player: " + req.params.player_id);
	Player.findById(req.params.player_id).select("-password")
	.then(player => {
		if(!player) {
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
			});
		}
		res.send(player);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve player with id " + req.parms.player_id
		});
	});
};

// Update a player
exports.update = (req, res) => {
	console.log("Received PUT request for player:" + JSON.stringify(req.body));
	//Validate request
	if(!req.body) {
		console.log("Player cannot be updated");
		return res.status(400).send({
			message: "Player cannot be updated"
		});
	}

	// Find and update player with req.body
	console.log("Player: " + req.params.player_id + " is being set to " + JSON.stringify(req.body));
	Player.findByIdAndUpdate(req.params.player_id, {
		player: req.body.player.username
	}, {new: true})
	.then(player => {
		if(!player) {
			console.log("Player not found with id " + req.params.player_id);
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
			});
		}
		res.send(player);
	}).catch(err => {
		console.log("There was an error saving the player.");
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
			});
		}
		return res.status(500).send({
			message: "Unable to update player with id " + req.parms.player_id
		});
	});
};

// Delete a player by id
exports.delete = (req, res) => {
	Player.findByIdAndRemove(req.params.player_id)
	.then(player => {
		if(!player) {
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
			});
		}
		res.send({message: "Player successfully deleted."});
	}).catch(err => {
		if(err.kind === 'ObjectId' || err.name === 'NotFound') {
			return res.status(404).send({
				message: "Player not found with id " + req.params.player_id
		});
	}
	return res.status(500).send({
		message: "Deleted player with id " + req.params.player_id
	});
});
}
