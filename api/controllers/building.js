const Building = require('../models/building.js');
const Character = require('../models/character.js');
const Player = require('../models/player.js');
const Town = require('../models/town.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

async function finalise(world) {
  let buildings = await Building.find({world: world});
  for (i=0;i<buildings.length;i++) {
    let town = await Town.findById(buildings[i].town);
    if (town.player) {
      if (buildings[i].type == "tavern") {buildings[i].set('cutscene', {title: 'A journey to the pub', body: 'first-visit'})};
      if (buildings[i].type == "hall") {buildings[i].set('cutscene', {title: 'First day at the office', body: 'first-visit'})};
      if (buildings[i].type == "barracks") {buildings[i].set('cutscene', {title: 'Meeting the local heroes', body: 'first-visit'})};
    }
    let owner = await Character.findOne({realEstate: buildings[i]});
    buildings[i].set('owner', owner);
    buildings[i].save();
  }
}

exports.finalise = finalise;

async function generate(world, settlements) {
  // This will need to be made much more dynamic so that different settlements contain different types of building
  let buildings = [];
  for (i=0;i<settlements.length;i++) {
    let settlement = settlements[i];
    let hall = this.construct(world, settlement, {
      type: "hall",
      name: settlement.name + " " + name.capitalize(settlement.status) + " Hall",
      reputation: dice.normal(settlement.reputation, 1, 200),
      founded: dice.normal(700, settlement.founded, 752)
    });
    buildings.push(hall);
    let tavern = this.construct(world, settlement, {
      type: "tavern",
      name: name.tavern(world.settings.names),
      reputation: dice.normal(settlement.reputation, 1, 200),
      founded: dice.normal(700, settlement.founded, 752)
    });
    buildings.push(tavern);
    let barracks = this.construct(world, settlement, {
      type: "barracks",
      name: settlement.name + " Barracks",
      reputation: dice.normal(settlement.reputation, 1, 200),
      founded: dice.normal(700, settlement.founded, 752)
    });
    buildings.push(barracks);
    // populate Tiles
    for (j=0;j<settlement.tiles.length;j++) {
      let tile = settlement.tiles[j];
      if (tile.active && dice.boolean(settlement.reputation / 200) && j != 0) {
        let improvements = [];
        if (tile.terrain.building) {
          improvements.push(tile.terrain.building)
        };
        if (tile.resource.building) {
          improvements.push(tile.resource.building)
        };
        if (improvements[0]) {
          let selected = improvements.length < 1 ? improvements[0] : dice.random(improvements);
          let improvement = world.settings.buildings.filter(building => building.formType === selected)[0];
          let building = this.construct(world, settlement, {
            type:       improvement.formType,
            reputation: dice.normal(settlement.reputation, 1, 200),
            founded:    dice.normal(700, settlement.founded, 752),
            food:       improvement.food,
            production: improvement.production,
            gold:       improvement.gold,
            culture:    improvement.culture,
            description:improvement.description,
            location:   j
          });
          buildings.push(building);
        }
      }
    }
  }
  let promises = buildings.map((building) => {
    return building.save();
  });
  return await Promise.all(promises);
}

exports.generate = generate;

exports.construct = (world, settlement, params) => {
	return new Building({
		type:           params.type,
		name:           params.name           || params.type,
		level:          params.level          || 1,
		health:         params.health         || dice.normal(100, 1, 100),
		maxHealth:      params.maxHealth      || 100,
		reputation:     params.reputation     || settlement.reputation,
		owner:          params.owner          || null,
		stock:          params.stock          || [],
		founded:        params.founded        || settlement.founded,
		cutscene:       params.cutscene       || null,
    food:           params.food           || 0,
    production:     params.production     || 0,
    gold:           params.gold           || 0,
    culture:        params.culture        || 0,
    cost:           1,
    progress:       null,
    location:       params.location       || null,
    description:    params.description    || null,
		world: world,
		town: settlement
	})
},

// Create new building
exports.create = (req, res) => {

	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "building settings cannot be empty"
		});
	}

	// Create building
	console.log("Received POST request for new building: " + JSON.stringify(req.body.building, null, 2));
	let building = req.body.building;
	const newbuilding= new Building({
		name: 		 	    building.name,
		type: 		 	    building.type,
		level:		 	    building.level,
		health: 	 	    building.health,
		maxHealth: 	    building.maxHealth,
		reputation:	    building.reputation,
		world:			    building.world,
		town: 			    building.town,
		owner:          building.owner,
		staff:			    building.staff,
		stock: 			    building.stock,
		cutscene:		    building.cutscene,
    food:           building.food,
    production:     building.production,
    gold:           building.gold,
    culture:        building.culture,
    cost:           building.cost,
    progress:       building.progress,
    needsTile:      building.needsTile,
    location:       building.location,
    description:    building.description,
		founded:	      building.founded,
    constructing:   building.constructing,
    repairing:      building.repairing
	});

	// Save building in database
	newbuilding.save().then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save building."
		});
	});
};

// Retrieve all buildings from the database
exports.findAll = (req, res) => {
	console.log("received GET request for buildings: " + JSON.stringify(req.query, null, 2));
	if (req.query) {
		Building.find(req.query)
		.then(buildings => {
			res.send(buildings);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to find any buildings with query: " + req.query
			});
		});
	} else {
		Building.find()
		.then(buildings => {
			res.send(buildings);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to list buildings."
			});
		});
	}
};

// Find a single building by id
exports.findOne = (req, res) => {
	console.log("received GET request for building: " + req.params.building_id);
	Building.findById(req.params.building_id)
	.then(building => {
		if(!building) {
			return res.status(404).send({
				message: "building not found with id " + req.params.building_id
			});
		}
		res.send(building);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "building not found with id " + req.params.building_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve building with id " + req.parms.building_id
		});
	});
};

// Update a building
exports.update = (req, res) => {
	console.log("Received PUT request for building:" + JSON.stringify(req.body, null, 2));
	//Validate request
	if(!req.body) {
		console.log("building content cannot be empty");
		return res.status(400).send({
			message: "building content cannot be empty"
		});
	}

	// Find and update building with req.params
	let building = req.body.building;
	Building.findByIdAndUpdate(req.params.building_id, {
		name: 		 	    building.name,
		type: 		 	    building.type,
		level:		 	    building.level,
		health: 	 	    building.health,
		maxHealth: 	    building.maxHealth,
		reputation:	    building.reputation,
		world:			    building.world,
		town: 			    building.town,
		owner:          building.owner,
		staff:			    building.staff,
		stock: 			    building.stock,
		cutscene:		    building.cutscene,
    food:           building.food,
    production:     building.production,
    gold:           building.gold,
    culture:        building.culture,
    cost:           building.cost,
    progress:       building.progress,
    needsTile:      building.needsTile,
    location:       building.location,
		founded:	      building.founded,
    constructing:   building.constructing,
    repairing:      building.repairing
	}, {new: true})
	.then(building => {
		if(!building) {
			console.log("building not found with id: "  + req.params.building_id);
			return res.status(404).send({
				message: "building not found with id: " + req.params.building_id
			});
		}
		res.send(building);
	}).catch(err => {
		console.log("There was an error saving the building.");
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "building not found with id " + req.params.building_id
			});
		}
		return res.status(500).send({
			message: "Unable to update building with id " + req.parms.building_id
		});
	});
};

// Delete a building by id
exports.delete = (req, res) => {
	Building.findByIdAndRemove(req.params.building_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted building with id: ' + req.params.building_id);
		res.send('Successfully deleted building with id: ' + req.params.building_id);
	})
};
