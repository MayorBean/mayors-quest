const Building = require('../models/building.js');
const buildingController = require('./building.js')
const Character = require('../models/character.js');
const characterController = require('./character.js')
const County = require('../models/county.js');
const countyController = require('./county.js')
const Mail = require('../models/mail.js');
const Player = require('../models/player.js');
const Region = require('../models/region.js');
const regionController = require('./region.js')
const Settlement = require('../models/town.js');
const settlementController = require('./town.js')
const World = require('../models/world.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

async function configure(world, player, settlements) {
	// Set player home to the smallest available village
	settlements = settlements.filter(settlement => !settlement.player)
	settlements.sort((a, b) => parseFloat(a.reputation) - parseFloat(b.reputation));
	let home = settlements[0];
	// Construct residence
	let residence = buildingController.construct(world, home, {
		type: "residence",
		name: "The " + name.capitalize(player.username) + " Residence",
		reputation: dice.normal(home.reputation, 1, 0),
		founded: dice.normal(700, home.founded, 752),
		cutscene: {title: "Your adventure begins", body: 'first-visit'}
	});
	residence.save();
	// Send intro mail
	let chancellor = await Character.findOne({home: home._id, profession: 'chancellor'});
	let mail = new Mail({
		world: world,
		recipient: player,
		sender: chancellor,
		town: home,
		date: {hour: world.date.hour, day: world.date.day, season: world.date.season, year: world.date.year},
		subject: "Welcome to " + home.name,
		content: 'welcome'
	});
	mail.save();
	home.set('mayor', undefined);
	home.set('player', player);
	return await home.save();
}

async function finalise(world, settlements, regions) {
	let capital = await Settlement.find({world: world, status: "capital"});
	let monarch = await characterController.generate(world, settlements, {
		profession: 'monarch',
		home: capital._id,
		dateOfBirth: characterController.getDateOfBirth(1, 100),
		reputation: 200,
	})
	monarch.save();
	world.set({
		capital: capital._id,
		monarch: monarch._id,
		regions: regions
	});
	return await world.save();
}

async function tutorial(world, settlement, date) {
	// If it's the second turn
	if (date.hour == 2 && date.day == 1 && date.season == 1 && date.year == 753) {
		let buildings = settlement.buildings;
		for (let i=0;i<buildings.length;i++) {
			buildings[i] = await Building.findById(buildings[i])
		}
		buildings = buildings.filter(building => building.type === 'hall' || 'barracks' || 'tavern');
		buildings = buildings.filter(building => {
			if (building.cutscene) {
				return building.cutscene.body === 'first-visit' ? true : false
			} else {
				return false;
			}
		})
		// check if player failed to visit any buildings
		if (buildings.length >= 3) {
			// activate the menu tip
			let residence = await Building.findOne({type: 'residence', town: settlement});
			residence.cutscene = {
				title: 'menu navigation',
				body: 'tutorial-navigation'
			}
			return residence;
		} else {return false}
	} else {return false}
}

async function processTurn(world, date) {
	world.set('date', date);
	regionController.setWeather(world);
	let settlements = await Settlement.find({world: world})
	for (let i=0; i < settlements.length; i++) {
		let settlement = settlements[i];
		if (settlement.project) {
			let buildings = await Building.find({town: settlement});
			let project = await Building.findById(settlement.project);
			settlement.useProduction(project, buildings, world, settlements);
		}
		if (date.hour == 1) {
			if (date.day == 1 || date.day == 8 || date.day == 15 || date.day == 22) {
				settlement.updateFinances(date);
			}
		}
		if (settlement.player) {
			let residence = await tutorial(world, settlement, date);
			if (residence) {await residence.save()}
		}
		settlement.save()
	}
	return await world.save();
}

async function generate(world) {
	let regions = await regionController.generate(world);
	let counties = await countyController.generate(world, regions);
	let settlements = await settlementController.generate(world, counties);
	let buildings = await buildingController.generate(world, settlements);
	let characters = await characterController.populate(world, settlements, buildings);
	await world.players.forEach((playerId) => {
		Player.findOne(playerId).then((player) => configure(world, player, settlements))
	});
	await buildingController.finalise(world)
	await settlementController.finalise(world);
	await countyController.finalise(counties);
	await regionController.finalise(regions);
	return await finalise(world, settlements, regions);
}

// Create new World
exports.create = (req, res) => {
	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "World settings cannot be empty"
		});
	}

	// Create World
	let post = req.body.world;
	console.log(`Player ${post.players[0]} is creating a new world called ${post.name}`);
	const world = new World({
		name:  post.name,
		date:  post.date || {hour: 1, day: 1, season: 1, year: 753},
		players: post.players,
		settings: post.settings,
		regions: post.regions,
		kingdom: name.kingdom(post.settings.names)
	});
	// Save World in database
	world.save()
	.then((world) => generate(world))
	.then((data) => res.send(data))
};

// Retrieve all worlds from the database
exports.findAll = (req, res) => {
	console.log("received GET request for worlds: " + JSON.stringify(req.query, null, 2));
	if (req.query) {
		World.find(req.query)
		.then(worlds => {
			res.send(worlds);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to find any worlds matching query: " + req.query
			});
		});
	} else {
		World.find()
		.then(worlds => {
			res.send(worlds);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to list worlds."
			});
		});
	}
};

// Find a single World by id
exports.findOne = (req, res) => {
	console.log("received GET request for world: " + req.params.world_id);
	World.findById(req.params.world_id)
	.then(world => {
		if(!world) {
			return res.status(404).send({
				message: "World not found with id " + req.params.world_id
			});
		}
		res.send(world);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "World not found with id " + req.params.world_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve world with id " + req.params.world_id
		});
	});
};

// Update a world
exports.update = (req, res) => {
	//Validate request
	if(!req.body) {
		console.log("Empty PUT request received for world: " + req.params.world_id);
		return res.status(400).send({
			message: "World content cannot be empty."
		});
	}
	console.log(`${req.body.world.name} is being updated`);
	// Find and update world with req.body
	World.findById(req.params.world_id)
	.then((world) => {
		if (world.date != req.body.world.date) {
			processTurn(world, req.body.world.date).then(world => {
				res.send(world);
			})
			;
		}	else {
			World.findByIdAndUpdate(req.params.world_id, {
				name: req.body.world.name,
				date: req.body.world.date,
				players: req.body.world.players,
				settings: req.body.world.settings,
				regions: req.body.world.regions,
				kingdom: req.body.world.kingdom,
				monarch: req.body.world.monarch,
				capital: req.body.world.capital
			}, {new: true})
			.then(world => {res.send(world);})
			.catch(err => {
				console.error("There was an error saving the world.");
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "World not found with id " + req.params.world_id
					});
				}
				return res.status(500).send({
					message: "Unable to update world with id " + req.params.world_id + ": " + err
				});
			});
		}
	})
};

// Delete a world by id
exports.delete = (req, res) => {
  Building.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all buildings in world: ' + req.params.world_id);
	});
	Character.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all characters in world: ' + req.params.world_id);
	});
	County.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all counties in world: ' + req.params.world_id);
	});
	Mail.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all mail in world: ' + req.params.world_id);
	});
	Region.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all regions in world: ' + req.params.world_id);
	});
	Settlement.deleteMany({world: req.params.world_id }, (err) => {
		if (err) return err;
		console.log('Removed all settlements in world: ' + req.params.world_id);
	});
	World.findByIdAndRemove(req.params.world_id, (err) => {
		if (err) return err;
		console.error('Deleted world with id: ' + req.params.world_id);
		res.send({message: 'Deleted world with id: ' + req.params.world_id});
	})
};
