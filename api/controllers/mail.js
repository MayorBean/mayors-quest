const Mail = require('../models/mail.js');
const name = require('../helpers/name.js');

// Create new mail
exports.create = (req, res) => {

	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "Mail contents cannot be empty"
		});
	}

	// Create mail
	console.log("Received POST request for new mail: " + JSON.stringify(req.body.mail));
	let mail = req.body.mail;
	const newMail = new Mail({
		world: mail.world,
		subject:  mail.subject,
		sender: mail.sender,
		recipient: mail.recipient,
		content: mail.content,
		date:  mail.date,
		responses: mail.responses,
		buildings: mail.buildings,
		characters: mail.buildings,
		unread: true
	});

	// Save mail in database
	newMail.save().then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save mail."
		});
	});
};

// Retrieve all messages from the database
exports.findAll = (req, res) => {
	console.log("received GET request for mail: " + JSON.stringify(req.query));
	if (req.query) {
		Mail.find(req.query)
		.then(messages => {
			res.send(messages);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "No mail found."
			});
		});
	} else {
		Mail.find()
		.then(messages => {
			res.send(messages);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to retrieve mail."
			});
		});
	}
};

// Find a single mail by id
exports.findOne = (req, res) => {
	console.log("received GET request for mail: " + req.params.mail_id);
	Mail.findById(req.params.mail_id)
	.then(mail => {
		if(!mail) {
			return res.status(404).send({
				message: "Message with id " + req.params.mail_id + " not found."
			});
		}
		res.send(mail);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Message with id " + req.params.mail_id + " not found."
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve mail with id " + req.parms.mail_id
		});
	});
};

// Update a mail
exports.update = (req, res) => {

	//Validate request
	if(!req.body) {
		return res.status(400).send({
			message: "Mail content cannot be empty"
		});
	}

	// Find and update mail with req.body
	console.log("Received PUT request for mail:" + JSON.stringify(req.body.mail));
	let mail = req.body.mail;
	Mail.findByIdAndUpdate(req.params.mail_id, {
		world: mail.world,
		subject:  mail.subject,
		sender: mail.sender,
		recipient: mail.recipient,
		content: mail.content,
		date:  mail.date,
		responses: mail.responses,
		buildings: mail.buildings,
		characters: mail.buildings,
		unread: mail.unread
	}, {new: false})
	.then(mail => {
		if(!mail) {
			return res.status(404).send({
				message: "Mail not found with id " + req.params.mail_id
			});
		}
		res.send(mail);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Mail not found with id " + req.params.mail_id
			});
		}
		return res.status(500).send({
			message: "Unable to update message with id " + req.parms.mail_id
		});
	});
};

// Delete a message by id
exports.delete = (req, res) => {
	Mail.findByIdAndRemove(req.params.mail_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted message with id: ' + req.params.mail_id);
		res.send({message: 'Successfully deleted mesage with id: ' + req.params.mail_id});
	});
};
