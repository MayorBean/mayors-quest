const Region = require('../models/region.js');
const County = require('../models/county.js');
const Settlement = require('../models/town.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

async function generate(world) {
	let regions = [];
	for (let i=0; i<(world.settings.size + 3); i++) {
		let climate = dice.random(world.settings.climates);
		let region = new Region({
			name: name.region(world.settings.names),
			climate: climate,
			weather: dice.random(climate.weather.spring),
			world: world
		});
		regions.push(region);
	};
	let promises = regions.map((region) => {return region.save()});
	return await Promise.all(promises);
}

exports.generate = generate;

async function setWeather(world) {
	let regions = await Region.find({world: world});
	regions.forEach((region) => {
		if (world.date.season == 1) {region.set('weather', dice.random(region.climate.weather.spring))}
		if (world.date.season == 2) {region.set('weather', dice.random(region.climate.weather.summer))}
		if (world.date.season == 3) {region.set('weather', dice.random(region.climate.weather.autumn))}
		if (world.date.season == 4) {region.set('weather', dice.random(region.climate.weather.winter))}
		region.save();
	})
}

exports.setWeather = setWeather;

async function finalise(regions) {
	for (let i = 0; i < regions.length; i++) {
		let counties = await County.find({region: regions[i]});
		let settlements = await Settlement.find({region: regions[i]});
		let sortedSettlements = dice.sort('reputation', settlements);
		let capital = sortedSettlements[0]._id;
		regions[i].set({
			capital: capital,
			counties: counties
		});
		regions[i].save();
	}
}

exports.finalise = finalise;

// Create new Region
exports.create = (req, res) => {

	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "Region settings cannot be empty"
		});
	}

	// Create region
	console.log("Received POST request for new region: " + JSON.stringify(req.body.region, null, 2));
	let region = req.body.region;
	const newRegion = new Region({
		name:  region.name,
		world:  region.world,
		capital: region.capital,
		climate: region.climate,
		weather: region.weather,
		counties: region.counties
	});

	// Save Region in database
	newRegion.save().then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save region."
		});
	});
};

// Retrieve all Regions from the database
exports.findAll = (req, res) => {
	console.log("received GET request for regions: " + JSON.stringify(req.query, null, 2));
	if (req.query) {
		Region.find(req.query)
		.then(regions => {
			res.send(regions);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to find any regions with query: " + req.query
			});
		});
	} else {
		Region.find()
		.then(regions => {
			res.send(regions);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to list regions."
			});
		});
	}
};

// Find a single region by id
exports.findOne = (req, res) => {
	console.log("received GET request for region: " + req.params.region_id);
	Region.findById(req.params.region_id)
	.then(region => {
		if(!region) {
			return res.status(404).send({
				message: "region not found with id " + req.params.region_id
			});
		}
		res.send(region);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "region not found with id " + req.params.region_id
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve region with id " + req.parms.region_id
		});
	});
};

// Update a Region
exports.update = (req, res) => {
	//Validate request
	if(!req.body) {
		console.log("Region content cannot be empty");
		return res.status(400).send({
			message: "Region content cannot be empty"
		});
	}

	// Find and update region with req.params
	console.log("region: " + req.params.region_id + " is being set to " + JSON.stringify(req.body, null, 2));
	let region = req.body.region;
	Region.findByIdAndUpdate(req.params.region_id, {
		name:  region.name,
		world:  region.world,
		capital: region.capital,
		climate: region.climate,
		counties: region.counties
	}, {new: true})
	.then(region => {
		if(!region) {
			console.log("Region not found with id: "  + req.params.region_id);
			return res.status(404).send({
				message: "Region not found with id: " + req.params.region_id
			});
		}
		res.send(region);
	}).catch(err => {
		console.log("There was an error saving the region.");
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Region not found with id " + req.params.region_id
			});
		}
		return res.status(500).send({
			message: "Unable to update region with id " + req.params.region_id + ": " + err
		});
	});
};

// Delete a region by id
exports.delete = (req, res) => {
	Region.findByIdAndRemove(req.params.region_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted region with id: ' + req.params.region_id);
		res.send('Successfully deleted region with id: ' + req.params.region_id);
	})
};
