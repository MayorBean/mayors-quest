const Town = require('../models/town.js');
const Building = require('../models/building.js');
const buildingController = require('../controllers/building.js');
const Character = require('../models/character.js');
const characterController = require('../controllers/character.js');
const World = require('../models/world.js');
const Region = require('../models/region.js');
const County = require('../models/region.js');
const Mail = require('../models/mail.js');
const dice = require('../helpers/dice.js');
const name = require('../helpers/name.js');

async function finalise(world) {
	let mayors = await Character.find({profession: "mayor", world: world})
	for (let i=0; i<mayors.length; i++) {
		let settlement = await Town.findById(mayors[i].home._id);
		let buildings = await Building.find({town: settlement});
		let improvements = buildings.filter(building => building.location != null);
		for (let j=0; j<improvements.length; j++) {
			let improvement = improvements[j];
			let owner = await Character.findById(improvement.owner._id);
			improvement.set('name', `${owner.firstName}'s ${improvement.type}`);
			improvement.save();
			settlement.tiles[improvement.location].set('building', improvement._id);
			settlement.tiles[improvement.location].set('owner', improvement.owner);
		}
		if (settlement.player) {
			mayors[i].remove();
		} else {
			settlement.mayor = mayors[i];
		}
		settlement.buildings = buildings
		settlement.save();
	}
}

function isTileActive(status, i) {
	let quantity;
	if (status === 'village') {quantity = 5}
	if (status === 'town') 		{quantity = 9}
	if (status === 'city') 		{quantity = 13}
	if (status === 'capital') {quantity = 21}
	if (i < quantity) {return true}
	else {return false}
}

function getResource(world, resourceChance, resources) {
	if (dice.boolean(resourceChance) && resources.length) {
		resourceType = dice.random(resources);
		return world.settings.resources.filter(resource => resource.name === resourceType)[0];
	}	else {return null}
}

function getTiles(world, status, reputation, biome, climate) {
	let tiles = [];
	let terrainChance, resourceType, output, resource, key, active;
	for (let i=0; i<25; i++) {
		if (i == 0) {
			terrainChance = 0.8;
			resourceChance = 0.4
		}
		else {
			// coast biomes generate more diverse terrain so coastlines don't look too stupid
			terrainChance = biome == 'coast' ? 0.8 : 0.4;
			resourceChance = 0.2;
		}
		// use biome as default terrain or select random terrain from settings
		key = dice.boolean(terrainChance) ? dice.random(biome.terrains) :  biome.name;
		let terrain = world.settings.terrains.filter(terrain => terrain.name === key)[0];
		let tile = {
			terrain: terrain,
			resource: getResource(world, resourceChance, terrain.resources),
			active: isTileActive(status, i)
		}
		tiles.push(tile);
	}
	return tiles;
}

// cookBooks randomly sets a settlement's finances during world generation
function cookBooks(world, status, reputation, population) {
	return [{
		balance: dice.roll(Math.round(reputation / 3 * population)) - dice.roll(Math.round((reputation / 3 * population) / 2)),
		debt: 0, // All settlements currently start with no debt. Consider modifying this later
		date: world.date
	}]
}

async function generate(world, counties) {
	let settlements = [];
	for (let i=0; i < ((world.settings.size + 3) * 25); i++) {
		let county = dice.random(counties);
		let region = await Region.findById(county.region);
		if (i == 1) {
			settlements.push(this.construct(world, region, county, {status: "capital", founded: 0}));
		}	else {
			settlements.push(this.construct(world, region, county, {status: dice.random(["city", "town", "town", "town", "village", "village", "village", "village", "village", "village"])}));
		}
	}
	let promises = settlements.map((settlement) => {return settlement.save()})
	return await Promise.all(promises);
}

exports.generate = generate;
exports.finalise = finalise;

exports.construct = (world, region, county, params) => {
	let attributes = this.rollAttributes(params.status, world);
	let founded = params.status === "capital" ? 0 : dice.roll(743);
	return new Town({
		name: params.name || name.town(world.settings.names),
		status: params.status || "village",
		world: params.world || world,
		founded: founded,
		climate: params.climate || region.climate.name,
		biome: params.biome || county.biome.name,
		reputation: params.reputation || attributes[0],
		population: params.population || attributes[1],
		finances: params.finances || attributes[2],
		tiles: params.tiles || getTiles(world, params.status, attributes[0], county.biome, region.climate),
		region: region,
		county: county
	});
},

exports.rollAttributes = (status, world) => {
	if (status == "village") {
		let reputation = 24 + dice.roll(50);
		let population = 100 + dice.roll(5000);
		let finances = cookBooks(world, status, reputation, population);
		return [reputation, population, finances];
	} else if (status == "town") {
		let reputation = 74 + dice.roll(50);
		let population = 5000 + dice.roll(50000);
		let finances = cookBooks(world, status, reputation, population);
		return [reputation, population, finances];
	} else if (status == "city") {
		let reputation = 124 + dice.roll(50);
		let population = 50000 + dice.roll(750000);
		let finances = cookBooks(world, status, reputation, population);
		return [reputation, population, finances];
	} else if (status == "capital") {
		let reputation = 174 + dice.roll(25);
		let population = 1000000 + (dice.roll(100000) - dice.roll(100000));
		let finances = cookBooks(world, status, reputation, population);
		return [reputation, population, finances];
	}
}

// Create new town
exports.create = (req, res) => {

	// Request validation
	if(!req.body) {
		return res.status(400).send({
			message: "Town contents cannot be empty"
		});
	}

	// Create town
	console.log("Received POST request for new town: " + JSON.stringify(req.body.town, null, 2));
	let body = req.body.town;
	const town = new Town({
		name: 						body.name,
		status: 					body.status,
		world: 						body.world,
		region: 					body.region,
		county: 					body.county,
		founded: 					body.founded,
		population: 			body.population,
		characters: 			body.characters,
		player: 					body.player,
		mayor: 						body.mayor,
		reputation: 			body.reputation,
		climate: 					body.climate,
		biome: 						body.biome,
		landform: 				body.landform,
		finances: 				body.finances,
		tiles: 					  body.tiles,
		project:  				body.project,
		buildings: 			  body.buildings
	});

	// Save town in database and update capital property of world/region/county
	town.save().then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Unable to save town."
		});
	});
};

// Retrieve all towns from the database
exports.findAll = (req, res) => {
	console.log("received GET request for town: " + JSON.stringify(req.query, null, 2));
	if (req.query) {
		Town.find(req.query).then(messages => {
			res.send(messages);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "No town found."
			});
		});
	} else {
		Town.find().then(messages => {
			res.send(messages);
		}).catch(err => {
			res.status(500).send({
				message: err.message || "Unable to retrieve town."
			});
		});
	}
};

// Find a single town by id
exports.findOne = (req, res) => {
	console.log("received GET request for town: " + req.params.town_id);
	Town.findById(req.params.town_id)
	.then(town => {
		if(!town) {
			return res.status(404).send({
				message: "Town with id " + req.params.town_id + " not found."
			});
		}
		res.send(town);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Message with id " + req.params.town_id + " not found."
			});
		}
		res.status(500).send({
			message: err.message || "Unable to retrieve town with id " + req.parms.town_id
		});
	});
};

// Update a town
exports.update = (req, res) => {
	//Validate request
	if(!req.body) {
		return res.status(400).send({
			message: "Town content cannot be empty"
		});
	}

	// Find and update town with req.body
	console.log("Received PUT REQUEST for town " + req.params.town_id + ":" + JSON.stringify(req.body, null, 2));
	let town = req.body.town;
	Town.findByIdAndUpdate(req.params.town_id, {
		name: town.name,
		status: town.status,
		world: town.world,
		region: town.region,
		county: town.county,
		founded: town.founded,
		population: town.population,
		mayor: town.mayor,
		reputation: town.reputation,
		climate: town.climate,
		biome: town.biome,
		landform: town.landform,
		finances: town.finances,
		player: town.player,
		tiles: town.tiles,
		project: town.project,
		buildings: town.buildings
	}, {new: true})
	.then(town => {
		if(!town) {
			return res.status(404).send({
				message: "Town not found with id " + req.params.town_id
			});
		}
		res.send(town);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Town not found with id " + req.params.town_id
			});
		}
		return res.status(500).send({
			message: "Unable to update message with id " + req.parms.town_id
		});
	});
};

// Delete a town by id
exports.delete = (req, res) => {
	Town.findByIdAndRemove(req.params.town_id, (err) => {
		if (err) return err;
		console.log('Successfully deleted town with id: ' + req.params.town_id);
		res.send('Successfully deleted town with id: ' + req.params.town_id);
	})
};
