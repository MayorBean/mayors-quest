const express = require('express');
const bodyParser = require('body-parser');
const { promisify } = require('util')
const fs = require('fs');
const bcrypt = require('bcrypt');
const cors = require('cors');
const app = express();

// Configuring the database
const config = require('./config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(config.url, {
	useCreateIndex: true,
	useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to MongoDB.");
}).catch(err => {
    console.log('Could not connect to MongoDB. Exiting now...', err);
    process.exit();
});

//use config module to get the privatekey, if no private key set, end the application
if (!config.privatekey) {
  console.error("FATAL ERROR: privatekey is not defined.");
  process.exit(1);
}

// parse requests
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//Enable CORS for all HTTP methods
app.use(cors());

// default route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to Mayor's Quest API"});
});

// Routes
require('./routes/building.js')(app);
require('./routes/character.js')(app);
require('./routes/county.js')(app);
require('./routes/mail.js')(app);
require('./routes/player.js')(app);
require('./routes/region.js')(app);
require('./routes/token.js')(app);
require('./routes/town.js')(app);
require('./routes/world.js')(app);

// listen on $port
app.listen(config.serverport, () => {
    console.log("Server is listening on port " + config.serverport);
});
