const mongoose = require('mongoose');
const dice = require('../helpers/dice.js');
const ObjectId = mongoose.Schema.Types.ObjectId;
const BuildingSchema = require('./building').schema;
const WorldSchema = require('./world').schema;
const RegionSchema = require('./region').schema;
const CountySchema = require('./county').schema;
const CharacterSchema = require('./character').schema;

const PlayerSchema = require('./player').schema;

const TownSchema = mongoose.Schema({
	name: 						String,
	status: 					String,
	population:  			Number,
	reputation:  			Number,
	climate:					String,
	biome:						String,
	finances: 				[{
		balance: 				Number,
		debt: 					Number,
		date:						{
			hour: 				Number,
			day:					Number,
			season:				Number,
			year:					Number,
		}
	}],
	player:						{type: ObjectId, ref: 'Player'},
	world: 						{type: ObjectId, ref: 'World'},
	region: 					{type: ObjectId, ref: 'Region'},
	county:	  				{type: ObjectId, ref: 'County'},
	mayor:						{type: ObjectId, ref: 'Character'},
	buildings:				[{type: ObjectId, ref: 'Building'}],
	project:					{type: ObjectId, ref: 'Building'},
	tiles:						[{
		active:					Boolean,
		owner:					{type: ObjectId, ref: 'Character'},
		building:				{type: ObjectId, ref: 'Building'},
		location:				String,
		terrain:				{
			name: 				String,
			building:			String,
			production: 	Number,
			culture:			Number,
			gold:					Number,
			food:					Number
		},
		resource: 		{
			name:					String,
			building:			String,
			production: 	Number,
			culture:			Number,
			gold:					Number,
			food:					Number
		},
	}],
	founded:   				Number
}, {
	timestamps: true
})

TownSchema.methods.updateFinances = function(date) {
	let profit = 2 - (Math.random() * 4); // random value until economy update
	if (this.finances[0].balance == 0 || this.finances[0].balance == 1) {this.finances[0].balance += 100};
	let balance = this.finances[0].balance + ((this.finances[0].balance * profit));
	let statement = {
		balance: Math.round(balance),
		debt: 0, // no debt until economy update
		date: date
	}
	this.finances.unshift(statement);
}

// useProduction applies a settlement's generated production to the specified construction project.
TownSchema.methods.useProduction = function(project, buildings, world, settlements) {
	let production = this.getProduction(buildings);
	// if project has a founded date assume it's being repaired
	project.founded ?
		this.repairBuilding(project, production, world)
		:
		this.constructBuilding(project, production, world, settlements);
	project.save();
}

// getProduction returns the total production generated by a given settlement
TownSchema.methods.getProduction = function(buildings) {
	let production = 1; //Set initial production to 1 in case settlement has no source of production
	this.tiles.forEach(tile => {
		if (tile.active) {
			if (tile.resource.production) {production += tile.resource.production};
			if (tile.terrain.production) {production += tile.terrain.production};
		}
	})
	buildings.forEach(building => {
		if (building.production) {production += building.production}
	})
	return production;
}

TownSchema.methods.repairsComplete = async function(project, world) {
	project.health = project.maxHealth;
	this.project = null;
	if (this.player) {
		let blueprint = world.settings.buildings.find(building => building.formType === project.type);
		let proprietor = await characterController.findOne({home: this, profession: blueprint.profession});
		let mail = new Mail({
			world: world,
			recipient: this.player,
			unread: true,
			sender: proprietor,
			town: this,
			date: {hour: world.date.hour, day: world.date.day, season: world.date.season, year: world.date.year},
			subject: `Repairs to ${project.name} complete`,
			buildings: [this.project],
			content: 'repairs-complete'
		});
		mail.save();
	}
}

TownSchema.methods.repairBuilding = function(project, production, world) {
	project.health += production;
	if (project.health >= project.maxHealth) {
		this.repairsComplete(project, world);
	}
}

TownSchema.methods.constructBuilding = function(project, production, world, settlements) {
	project.progress += production;
	if (project.progress >= project.cost) {
		this.constructionComplete(project, world, settlements);
	}
}

TownSchema.methods.constructionComplete = async function(project, world, settlements) {
	project.progress = project.cost;
	project.founded = world.date.year;
	this.project = null;
	// Generate new proprietor
	let blueprint = world.settings.buildings.find(building => building.formType === project.type);
	let profession = blueprint.profession;
	let proprietor = characterController.generate(world, settlements, {
		profession: profession,
		home: settlement,
		dateOfBirth: characterController.getDateOfBirth(20, 75),
		reputation: dice.normal(settlement.reputation, 1, 200),
		realEstate: project
	});
	await proprietor.save();
	project.owner = proprietor;
	if (settlement.player) {
		let mail = new Mail({
			world: world,
			recipient: settlement.player,
			unread: true,
			sender: proprietor,
			town: settlement,
			date: {hour: world.date.hour, day: world.date.day, season: world.date.season, year: world.date.year},
			subject: `A quick introduction`,
			buildings: [project],
			content: 'introduction'
		});
		mail.save();
	}
}

module.exports = mongoose.model('Towns', TownSchema);
