const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const PlayerSchema = require('./player').schema;
const TownSchema = require('./town').schema;
const BuildingSchema = require('./building').schema;
const WorldSchema = require('./world').schema;

const CharacterSchema = mongoose.Schema({
  world:               { type: ObjectId, ref: 'World' },
  firstName:           { type: String },
  lastName:            { type: String },
  gender:              { type: String },
  birthplace:          { type: ObjectId, ref: 'Town' },
  home:                { type: ObjectId, ref: 'Town' },
  location:            { type: ObjectId, ref: 'Town' },
  profession:          { type: String },
  class:               {
    name:              { type: String },
    majorAttributes:   [{ type: String }],
    minorAttributes:   [{ type: String }],
    skills:            [{}]
  },
  level:               { type: Number},
  trait:               { type: String, default: 'none' },
  morale:              { type: String, default: 'okay' },
  status:              { type: String, default: 'normal' },
  dateOfBirth:         {
  	hour:              { type: Number, min: 1 },
  	day:               { type: Number, min: 1 },
  	season:            { type: Number, min: 1 },
  	year:              { type: Number, min: 1 },
  },
  experience:          { type: Number, default: 0 },
  reputation:          { type: Number, default: 1, min: 1, max: 200 },
  potential:           { type: Number, default: 1, min: 1, max: 200 },
  ability:             { type: Number, default: 1, min: 1, max: 200 },
  physicalAttributes:  {
    agility:           { type: Number, min: 0, max: 20 },
    balance:           { type: Number, min: 0, max: 20 },
    dexterity:         { type: Number, min: 0, max: 20 },
    reflexes:          { type: Number, min: 0, max: 20 },
    speed:             { type: Number, min: 0, max: 20 },
    stamina:           { type: Number, min: 0, max: 20 },
    strength:          { type: Number, min: 0, max: 20 },
  },
  mentalAttributes:    {
    bravery:           { type: Number, min: 0, max: 20 },
    charisma:          { type: Number, min: 0, max: 20 },
    composure:         { type: Number, min: 0, max: 20 },
    creativity:        { type: Number, min: 0, max: 20 },
    intelligence:      { type: Number, min: 0, max: 20 },
    leadership:        { type: Number, min: 0, max: 20 },
    wisdom:            { type: Number, min: 0, max: 20 },
  },
  hiddenAttributes:    {
    adaptability:      { type: Number, min: 0, max: 20 },
    ambition:          { type: Number, min: 0, max: 20 },
    chivalry:          { type: Number, min: 0, max: 20 },
    discipline:        { type: Number, min: 0, max: 20 },
    honor:             { type: Number, min: 0, max: 20 },
    loyalty:           { type: Number, min: 0, max: 20 },
    versatility:       { type: Number, min: 0, max: 20 },
  },
  playersMet:          [{ type: ObjectId, ref: 'Player'}],
  realEstate:          { type: ObjectId, ref: 'Building'},
  //inventory:           [{ type: ObjectId, ref: 'Item' }],
  //armour:            { type: Object },
  //weapon:            { type: Object },
  //stats:             { type: Object },
  }, {
	timestamps: true
});

module.exports = mongoose.model('Character', CharacterSchema);
