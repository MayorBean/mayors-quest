const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const RegionSchema = require('./region').schema;
const TownSchema = require('./town').schema;
const WorldSchema = require('./world').schema;

const CountySchema = mongoose.Schema({
	name: String,
	region: { type: ObjectId, ref: 'Region'},
	capital: { type: ObjectId, ref: 'Town'},
	world: { type: ObjectId, ref: 'World' },
	biome: {
		terrains: [String],
		name: String
	}
}, {
	timestamps: true
})

module.exports = mongoose.model('Counties', CountySchema);
