const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const WorldSchema = require('./world').schema;
const uniqueValidator = require('mongoose-unique-validator');

const PlayerSchema = mongoose.Schema({
	username: {
		type: String,
		lowercase: true,
		required: [true, "can't be blank"],
		match: [/^[a-zA-Z0-9]+$/, "is invalid"],
		unique: true,
		index: true,
		minlength: 3,
		maxlength: [20, "too long"]
	},
	password: {
		type: String,
		required: true,
		minlenth: 3,
		maxlength: 60,
	}
}, {
	timestamps: true
});

PlayerSchema.methods = {
  comparePassword: function(candidatePassword, cb) {
      bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
          if (err) return cb(err);
          cb(null, isMatch);
      });
  }
}

PlayerSchema.plugin(uniqueValidator, {message: 'is already taken.'});

module.exports = mongoose.model('Players', PlayerSchema);
