const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const PlayerSchema = require('./player').schema;
const BuildingSchema = require('./building').schema;
const TownSchema = require('./town').schema;
const WorldSchema = require('./world').schema;
const CharacterSchema = require('./character').schema;

const MailSchema = mongoose.Schema({
	world: { type: ObjectId, ref: 'World' },
	subject:  String,
	sender: { type: ObjectId, ref: 'Character' },
	recipient: { type: ObjectId, ref: 'Player' },
	town: { type: ObjectId, ref: 'Town'},
	buildings: [{type: ObjectId, ref: 'Building'}],
	characters: [{type: ObjectId, ref: 'Character'}],
	content: String,
	date: {
		hour: Number,
		day: Number,
		season: Number,
		year: Number,
	},
	unread: { type: Boolean, default: true },
	responses: [{}]
}, {
	timestamps: true
})

module.exports = mongoose.model('Mail', MailSchema);
