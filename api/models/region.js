const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const WorldSchema = require('./world').schema;
const CountySchema = require('./county').schema;

const RegionSchema = mongoose.Schema({
	name: String,
	world: {type: ObjectId, ref: 'World'},
	capital: {type: ObjectId, ref: 'Town'},
	counties: [{type: ObjectId, ref: 'County'}],
	climate: {
		name: String,
		biomes: [String],
		weather: {
			spring: [String],
			summer: [String],
			autumn: [String],
			winter: [String]
		}
	},
	weather: String
}, {
	timestamps: true
})

module.exports = mongoose.model('Regions', RegionSchema);
