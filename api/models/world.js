const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const PlayerSchema = require('./player').schema;
const TownSchema = require('./town').schema;
const RegionSchema = require('./region').schema;
const CharacterSchema = require('./character').schema;

const WorldSchema = mongoose.Schema({
	name: String,
	players: [{ type: ObjectId, ref: 'Player' }],
	date: {
		hour: Number,
		day: Number,
		season: Number,
		year: Number,
	},
	settings: {
		name: String,
		author: String,
		size: Number,
		names: {
			female: [String],
			male: [String],
			prefixes: [String],
			suffixes: [String],
			kingdom_suffixes: [String],
			region_suffixes: [String],
			county_suffixes: [String],
			tavern_prefixes: [String],
			tavern_prefixes_plural: [String],
			tavern_suffixes: [String]
		},
		classes: [{
			name: String,
			majorAttributes: [String],
			minorAttributes: [String],
			skills: [{}]
		}],
		professions: [String],
		climates: [{
			name: String,
			weather: {
				spring: [String],
				summer: [String],
				autumn: [String],
				winter: [String]
			},
			biomes: [String]
		}],
		biomes: [{
			name: String,
			terrains: [String]
		}],
		terrains: [{
			name: String,
			food: Number,
			production: Number,
			gold: Number,
			culture: Number,
			resources: [String],
			building: String
		}],
		resources: [{
			name: String,
			food: Number,
			production: Number,
			gold: Number,
			culture: Number,
			building: String
		}],
		buildings: [{
			formType: String,
			profession: String,
			food: Number,
			production: Number,
			gold: Number,
			culture: Number,
			cost: Number,
			needsTile: Boolean,
			description: String
		}]
	},
	kingdom: String,
	regions: [{ type: ObjectId, ref: 'Region' }],
	capital: { type: ObjectId, ref: 'Town' },
	monarch: { type: ObjectId, ref: 'Character' },
}, {
	timestamps: true
})

WorldSchema.methods.incrementTime = function() {
	this.date.hour < 4 ? this.date.hour++ : this.date.hour = 1 && this.date.day++
	this.date.day < 28 ? this.date.day++ : this.date.day = 1 && this.date.season++
	this.date.season < 4 ? this.date.season++ : this.date.season = 1 && this.date.year++
},

module.exports = mongoose.model('Worlds', WorldSchema);
