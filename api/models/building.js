const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const CharacterSchema = require('./character').schema;
const PlayerSchema = require('./player').schema;
const TownSchema = require('./town').schema;
const WorldSchema = require('./world').schema;

const BuildingSchema = mongoose.Schema({
	name:        		String,
  type:        		String,
  level:       		Number,
  health:      		Number,
  maxHealth:   		Number,
	reputation:  		Number,
  world:      	  {type: ObjectId, ref: 'World'},
  town:       	  {type: ObjectId, ref: 'Town'},
  owner: 	  			{type: ObjectId, ref: 'Character'},
  stock:      	  [{type: ObjectId, ref: 'Item'}],
	cutscene:				{
		title: 				String,
		body:					String	
	},
	gold: 		 			Number,
	food:	       		Number,
	production:	 		Number,
	culture:				Number,
	cost: 					Number,
	progress:				Number,
	founded:   	 		Number,
	needsTile:			Boolean,
	location:				Number,
	repairing:			Boolean,
	constructing:		Boolean,
	description:		String
}, {
	timestamps: true
})

module.exports = mongoose.model('Buildings', BuildingSchema);
