module.exports = {
    url: 'mongodb://localhost:27017/mayors-quest',
    serverport: 3000,
    settingsPath: './data/default.json',
    privatekey: 'mayorskey'
}
