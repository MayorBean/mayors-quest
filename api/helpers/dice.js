exports.roll = (sides) => {
  return Math.floor(Math.random() * sides) + 1;
}

exports.sort = (prop, arr) => {
  // This helper sorts arrays into reverse order based on a given child property
  prop = prop.split('.');
  var len = prop.length;
  arr.sort(function (a, b) {
      var i = 0;
      while( i < len ) { a = a[prop[i]]; b = b[prop[i]]; i++; }
      if (a < b) {
          return 1;
      } else if (a > b) {
          return -1;
      } else {
          return 0;
      }
  });
  return arr;
};
exports.boolean = (chance) => {
  // Chance is a number between 0-1. 0.5 = 50% chance.
  return Math.random() <= (chance); // Returns TRUE if the random number is below the chance provided.
}
exports.random= (options) => {
  // Returns a random record from a given array
  return options[Math.floor(Math.random() * options.length)]
}
exports.normal = (mean, min, max) => {
  // Returns a random integer with a normalized probability. It clamps and rounds the integer before returning it.
  let result = ((Math.random() + Math.random() + Math.random() + Math.random() + Math.random() / 5) / 2) * mean;
  return Math.round(this.clamp(result, min, max));
}
exports.clamp = (number, min, max) => {
  return Math.min(Math.max(number, min), max);
}
