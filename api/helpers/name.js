const dice = require('./dice.js');

exports.capitalize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

exports.firstName = (names) => {
  // Whether the name is male or female is determined by which names object is passed to this function.
  return dice.random(names);
}
exports.kingdom = (forms) => {
  let prefix = dice.random(forms.prefixes);
  let suffix = dice.random(forms.kingdom_suffixes);
  if (suffix == "lands") {return "The " + this.capitalize(prefix + suffix)}
  return this.capitalize(prefix+suffix);
}
exports.region = (forms) => {
  let prefix = dice.random(forms.prefixes);
  let suffix = dice.random(forms.region_suffixes);
  if (suffix == "lands") {
    let roll = dice.roll(4);
    if (roll == 1) { return "North " + this.capitalize(prefix + suffix);}
    if (roll == 2) { return "East "  + this.capitalize(prefix + suffix);}
    if (roll == 3) { return "South " + this.capitalize(prefix + suffix);}
    if (roll == 4) { return "West "  + this.capitalize(prefix + suffix);}
  } else {
    return this.capitalize(prefix + suffix);
  }
}
exports.county = (forms) => {
  let prefix = dice.random(forms.prefixes);
  let suffix = dice.random(forms.county_suffixes);
  if (suffix == "lands") {
    let roll = dice.roll(4);
    if (roll == 1) { return "North " + this.capitalize(prefix + suffix);}
    if (roll == 2) { return "East "  + this.capitalize(prefix + suffix);}
    if (roll == 3) { return "South " + this.capitalize(prefix + suffix);}
    if (roll == 4) { return "West "  + this.capitalize(prefix + suffix);}
  } else {
    return this.capitalize(prefix + suffix);
  }
}
exports.town = (forms) => {
  let prefix = dice.random(forms.prefixes);
  let suffix = dice.random(forms.suffixes);
  return this.capitalize(prefix + suffix); // Requires regex to remove a letter if it is duplicated at the end of the prefix and start of the suffix
}
exports.tavern = (forms) => {
  if (dice.roll(5) > 4) {
    let prefix = dice.random(forms.tavern_prefixes_plural);
    let suffix = dice.random(forms.tavern_suffixes) + 's'; // Create a exports.that properly pluralizes strings with regex
    return "The " + this.capitalize(prefix) + ' ' + this.capitalize(suffix);
  } else {
    let prefix = dice.random(forms.tavern_prefixes);
    let suffix = dice.random(forms.tavern_suffixes);
    return "The " + this.capitalize(prefix) + ' ' + this.capitalize(suffix);
  }
}
