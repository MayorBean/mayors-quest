module.exports = (app) => {
	const worlds = require('../controllers/world.js');

	//Create new World
	app.post('/worlds', worlds.create);

	//Retrieve all Worlds
	app.get('/worlds', worlds.findAll);

	//Retrieve a single World by id
	app.get('/worlds/:world_id', worlds.findOne);

	//Update a World by id
	app.put('/worlds/:world_id', worlds.update);

	//Delete a World by id
	app.delete('/worlds/:world_id', worlds.delete);
}
