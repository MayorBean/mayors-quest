module.exports = (app) => {
	const regions = require('../controllers/region.js');

	//Create new region
	app.post('/regions', regions.create);

	//Retrieve all regions
	app.get('/regions', regions.findAll);

	//Retrieve a single region by id
	app.get('/regions/:region_id', regions.findOne);

	//Update a region by id
	app.put('/regions/:region_id', regions.update);

	//Delete a region by id
	app.delete('/regions/:region_id', regions.delete);
}
