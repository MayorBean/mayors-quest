module.exports = (app) => {
	const counties = require('../controllers/county.js');

	//Create new county
	app.post('/counties', counties.create);

	//Retrieve all counties
	app.get('/counties', counties.findAll);

	//Retrieve a single county by id
	app.get('/counties/:county_id', counties.findOne);

	//Update a county by id
	app.put('/counties/:county_id', counties.update);

	//Delete a county by id
	app.delete('/counties/:county_id', counties.delete);
}
