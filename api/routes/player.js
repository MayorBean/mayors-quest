module.exports = (app) => {
	const players = require('../controllers/player.js');

	//Create new player
	app.post('/players', players.create);

	//List all Players
	app.get('/players', players.findAll);

	//Find a player
	app.get('/players/:player_id', players.findById);

	//Update a player by id
	app.put('/players/:player_id', players.update);

	//Delete a World by id
	app.delete('/players/:player_id', players.delete);
}
