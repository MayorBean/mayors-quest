module.exports = (app) => {
	const characters = require('../controllers/character.js');

	//Create new character
	app.post('/characters', characters.create);

	//Retrieve all characters
	app.get('/characters', characters.findAll);

	//Retrieve a single character by id
	app.get('/characters/:character_id', characters.findOne);

	//Update a character by id
	app.put('/characters/:character_id', characters.update);

	//Delete a character by id
	app.delete('/characters/:character_id', characters.delete);
}
