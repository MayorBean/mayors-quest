module.exports = (app) => {
	const token = require('../controllers/token.js');

	//Create new token
	app.post('/token', token.create);

}
