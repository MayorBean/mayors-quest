module.exports = (app) => {
	const town = require('../controllers/town.js');

	//Create new town
	app.post('/towns', town.create);

	//Retrieve all town
	app.get('/towns', town.findAll);

	//Retrieve a single town by id
	app.get('/towns/:town_id', town.findOne);

	//Update a town by id
	app.put('/towns/:town_id', town.update);

	//Delete a town by id
	app.delete('/towns/:town_id', town.delete);
}
