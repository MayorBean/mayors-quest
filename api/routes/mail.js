module.exports = (app) => {
	const mail = require('../controllers/mail.js');

	//Create new World
	app.post('/mail', mail.create);

	//Retrieve all mail
	app.get('/mail', mail.findAll);

	//Retrieve a single World by id
	app.get('/mail/:mail_id', mail.findOne);

	//Update a World by id
	app.put('/mail/:mail_id', mail.update);

	//Delete a World by id
	app.delete('/mail/:mail_id', mail.delete);
}
