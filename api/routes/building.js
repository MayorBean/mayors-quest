module.exports = (app) => {
	const buildings = require('../controllers/building.js');

	//Create new county
	app.post('/buildings', buildings.create);

	//Retrieve all buildings
	app.get('/buildings', buildings.findAll);

	//Retrieve a single county by id
	app.get('/buildings/:building_id', buildings.findOne);

	//Update a county by id
	app.put('/buildings/:building_id', buildings.update);

	//Delete a county by id
	app.delete('/buildings/:building_id', buildings.delete);
}
