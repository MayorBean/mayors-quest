# Mayor's Quest

Mayor's Quest is a town management simulator set in a randomly generated fantasy world.

## Getting Started

These instructions assume you are running Ubuntu or a Linux distro with the APT package manager.

### Prerequisites

Dependencies needed to install the game:

* **NodeJS**: ```sudo apt install nodejs```
* **NPM**: ```sudo apt install npm```
* **MongoDB**: ``` sudo apt install mongodb```
* **Ember.js** ```npm install -g ember-cli```

### Installing

* ```git clone https://gitlab.com/MayorBean/mayors-quest.git```
* ```cd mayors-quest/api && npm install```
* ```cd ../```
* ```npm install```

### Running

* ```npm start```

The game can be run by visiting http://localhost:4200. The API can accessed at http://localhost:3000.

## Authors

* **Mayor Bean** - *Initial work* - [MayorBean](https://gitlab.com/MayorBean)

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by: Football Manager, Settlers, Heroes of Might & Magic, XCOM, Final Fantasy, Pokemon, Civilization, Quest for Glory, Speedball.
