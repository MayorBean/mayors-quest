import Session from 'ember-simple-auth/services/session';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Session.extend({
  store: service(),
  player: computed('isAuthenticated', function() {
    const playerId = this.get('session.content.authenticated._id');
    if (!isEmpty(playerId)) {
      return this.get('store').findRecord('player', playerId);
    }
  })
});
