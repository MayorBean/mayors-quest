import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    return (DS.typeOf(serialized) == "array")
        ? serialized
        : [];
  },

  serialize(deserialized) {
    var type = DS.typeOf(deserialized);
    if (type == 'array') {
        return deserialized
    } else if (type == 'string') {
        return deserialized.split(',').map(function(item) {
            return item.trim();
        });
    } else {
        return [];
    }
  }
});
