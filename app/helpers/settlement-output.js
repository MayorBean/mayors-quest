import { helper } from '@ember/component/helper';

export default helper(function settlementOutput(params/*, hash*/) {
  let output = {food: 1, production: 1, gold: 1, culture: 1}; // base stats for every settlement.
  let tiles = params[0].tiles;
  let key = params[1];
  tiles.forEach(tile => {
    if (tile.active) {
      output.food += tile.resource ? (tile.resource.food + tile.terrain.food) : tile.terrain.food;
      output.production += tile.resource ? (tile.resource.production + tile.terrain.production) : tile.terrain.production;
      output.gold += tile.resource ? (tile.resource.gold + tile.terrain.gold) : tile.terrain.gold;
      output.culture += tile.resource ? (tile.resource.culture + tile.terrain.culture) : tile.terrain.culture;
    }
  });
  return output[key] || output;
});
