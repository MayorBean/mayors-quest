import { helper } from '@ember/component/helper';

export default helper(function characterPersonality(params/*, hash*/) {
  let attributes = params[0];
  let largest = Object.keys(attributes).reduce(function(largest,key){
    return (largest === undefined || attributes[key] > attributes[largest]) ? key : largest;
  });
  let smallest = Object.keys(attributes).reduce(function(smallest,key){
    return (smallest === undefined || attributes[key] < attributes[smallest]) ? key : smallest;
  });
  if (attributes[largest] < 14 && attributes[smallest] > 6) {return 'balanced'}
  if ((attributes[largest] - 10) >= (10 - attributes[smallest])) {
    if (largest === 'adaptability') {return 'confident'}
    if (largest === 'ambition') {return 'ambitious'}
    if (largest === 'chivalry') {return 'noble'}
    if (largest === 'discipline') {return 'serious'}
    if (largest === 'honor') {return 'honorable'}
    if (largest === 'loyalty') {return 'loyal'}
    if (largest === 'versatility') {return 'playful'}
  } else {
    if (smallest === 'adaptability') {return 'fussy'}
    if (smallest === 'ambition') {return 'unambitious'}
    if (smallest === 'chivalry') {return 'common'}
    if (smallest === 'discipline') {return 'casual'}
    if (smallest === 'honor') {return 'dishonorable'}
    if (smallest === 'loyalty') {return 'selfish'}
    if (smallest === 'versatility') {return 'stubborn'}
  }
});
