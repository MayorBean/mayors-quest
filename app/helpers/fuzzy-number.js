import { helper } from '@ember/component/helper';

export default helper(function fuzzyNumber(params/*, hash*/) {
  if (params === 1) {return 'a single'}
  if (params === 2) {return 'a couple of'}
  if (params < 12) {return 'a few'}
  if (params === 12) {return 'a dozen'}
  if (params < 90) {return 'dozens of'}
  if (params < 120) {return 'around a hundred'}
  if (params < 900) {return 'hundreds of'}
  if (params < 1200) {return 'around a thousand'}
  if (params < 9000) {return 'thousands of'}
  if (params < 12000) {return 'around ten thousand'}
  if (params < 90000) {return 'tens of thousands of'}
  if (params < 120000) {return 'around a hundred thousand'}
  if (params < 900000) {return 'hundreds of thousands'}
  if (params < 1200000) {return 'possibly a million'}
  return 'more than a million'
});
