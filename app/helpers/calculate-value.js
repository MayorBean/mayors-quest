import { helper } from '@ember/component/helper';

export function value(params) {
  return params[0] * params[1] * params[2];
}

export default helper(value);
