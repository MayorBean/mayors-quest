import { helper } from '@ember/component/helper';

export function dateFromNow(params/*, hash*/) {
  let currentDate = params[0];
  let query = params[1];
  if (currentDate.year == query.year) {
    if (currentDate.season == query.season) {
      if (currentDate.day == query.day) {
        if (currentDate.hour == query.hour) {
          return 'just now'
        } else {
          if (query.hour == 1) {return 'this morning'}
          if (query.hour == 2) {return 'this afternoon'}
          if (query.hour == 3) {return 'this evening'}
          return '???'
        }
      } else {
        if (currentDate.day - query.day == 1) {
          if (query.hour < 3) {return 'yesterday'}
          return 'last night'
        }
        if (currentDate.day - query.day == 2) {return 'a couple of days ago'}
        if (currentDate.day - query.day < 7) {return 'a few days ago'}
        if (currentDate.day - query.day < 14) {return 'a week ago'}
        return Math.round((currentDate.day - query.day) / 7) + ' weeks ago'
      }
    } else {
      if (currentDate.season - query.season == 1) {return 'last season'}
      return (currentDate.season - query.season) + ' seasons ago'
    }
  } else {
    if (currentDate.year - query.year == 1) {return 'last year'}
    return (currentDate.year - query.year) + ' years ago'
  }
}

export default helper(dateFromNow);
