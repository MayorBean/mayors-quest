import { helper } from '@ember/component/helper';

export function value(params) {
  let building = params[0];
  let condition = (building.health / building.maxHealth) * 100;
  let age = params[1].year - building.founded;
  if (age < 2) {
    if (condition == 100) {return `a suspiciously new building`}
    if (condition > 75) {return `a new building that already looks a bit worn down`}
    if (condition >= 50) {return `a new building that's badly charred from a recent fire`}
    if (condition < 50) {return `a new building, but it's already half falling down`}
  }
  if (age < 10) {
    if (condition == 100) {return `a building that's a few years old but seems to be in perfect condition`}
    if (condition > 75) {return `a building that's a few years old and looks it`}
    if (condition >= 50) {return `a building that's a few years old and is badly damaged`}
    if (condition < 50) {return `a building that's only a few years old but is already half falling down`}
  }
  if (age < 100) {
    if (condition == 100) {return `an old building that seems to have recently been renovated`}
    if (condition > 75) {return `an old building that could use some renovations`}
    if (condition >= 50) {return `an old building that has clearly seen better days`}
    if (condition < 50) {return `an old building that's barely still standing`}
  }
  if (age >= 100) {
    if (condition == 100) {return `an ancient building that's been lovingly restored and maintained`}
    if (condition > 75) {return `an ancient building that's in surprisingly good condition`}
    if (condition >= 50) {return `an ancient building that has clearly seen better days`}
    if (condition < 50) {return `an ancient building that's barely still standing`}
  }
}

export default helper(value);
