import { helper } from '@ember/component/helper';

export default helper(function constructionTime(params/*, hash*/) {
  let project = params[0];
  let production = params[1];
  let total = 0;
  if (project.get('founded')) {
    // repairing
    total = project.get('maxHealth') - project.get('health');
  } else {
    // constructing
    total = project.get('cost') - project.get('progress');
  }
  let turns = total / production;
  let days = Math.ceil(turns / 4)
  if (days > 1) {return `${days} days`}
if (days == 1) {return `${days} day`}
  if (days < 1) {return `done`}
});
