import { helper } from '@ember/component/helper';

export default helper(function indefiniteArticle(params/*, hash*/) {
  let word = JSON.stringify(params);
  return word.charAt(2).match(/[aeiouAEIOU]/) ? 'an' : 'a'
});
