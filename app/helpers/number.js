import { helper } from '@ember/component/helper';

export function number(params/*, hash*/) {
  let string = params.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  if (params < 0) {return `<span class="red">${string}</span>`}
  else {return string}
}

export default helper(number);
