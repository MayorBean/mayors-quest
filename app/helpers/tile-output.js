import { helper } from '@ember/component/helper';

export default helper(function tileOutput(params/*, hash*/) {
  let tiles = params[0];
  let selected = params[1];
  let tile = tiles[selected];
  let key = params[2];
  if (tile.active) {
    let output = tile.resource ? (tile.resource[key] + tile.terrain[key]) :tile.terrain[key];
    return output;
  } else {return 0}
});
