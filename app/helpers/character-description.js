import { helper } from '@ember/component/helper';

export default helper(function characterDescription(params/*, hash*/) {
  let gender = params[0].gender;
  let age = params[1];
  if (gender == "male") {
    if (age < 15) {return "a boy"}
    if (age < 25) {return "a young man"}
    if (age < 50) {return "a man"}
    if (age >= 50) {return "an old man"}
  } else {
    if (age < 15) {return "a girl"}
    if (age < 25) {return "a young woman"}
    if (age < 50) {return "a woman"}
    if (age >= 50) {return "an old woman"}
  }
});
