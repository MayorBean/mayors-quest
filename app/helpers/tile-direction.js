import { helper } from '@ember/component/helper';

export default helper(function tileDirection(params/*, hash*/) {
  let i = params[0];
  /*	Tile placement:
	----------------------
	| 24, 20, 09, 13, 21 |
	| 19, 08, 01, 05, 14 |
	| 12, 04, 00, 02, 10 |
	| 18, 07, 03, 06, 15 |
	| 23, 17, 11, 16, 22 |
	--------------------*/

	if (i == 0) {return 'centre'}
	if (i == 1) {return 'north'}
	if (i == 2) {return 'east'}
	if (i == 3) {return 'south'}
	if (i == 4) {return 'west'}
	if (i == 5) {return 'northeast'}
	if (i == 6) {return 'southeast'}
	if (i == 7) {return 'southwest'}
	if (i == 8) {return 'northwest'}
	if (i == 9) {return 'far north'}
	if (i == 10) {return 'far east'}
	if (i == 11) {return 'far south'}
	if (i == 12) {return 'far west'}
	if (i == 13) {return 'north-northeast'}
	if (i == 14) {return 'east-northeast'}
	if (i == 15) {return 'east-southeast'}
	if (i == 16) {return 'south-southeast'}
	if (i == 17) {return 'south-southwest'}
	if (i == 18) {return 'west-southwest'}
	if (i == 19) {return 'west-northwest'}
	if (i == 20) {return 'north-northwest'}
	if (i == 21) {return 'far northeast'}
	if (i == 22) {return 'far southeast'}
	if (i == 23) {return 'far southwest'}
	if (i == 24) {return 'far northwest'}
});
