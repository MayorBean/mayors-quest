import { helper } from '@ember/component/helper';

export function reputationString(reputation/*, hash*/) {
  if (reputation < 25) {return `basic`;}
  if (reputation < 50) {return `modest`;}
  if (reputation < 75) {return `okay`;}
  if (reputation < 100) {return `good`;}
  if (reputation < 125) {return `upscale`;}
  if (reputation < 150) {return `renowned`;}
  if (reputation < 175) {return `famous`;}
  if (reputation < 200) {return `legendary`;}
}

export default helper(reputationString);
