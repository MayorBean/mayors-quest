import { helper } from '@ember/component/helper';

export default helper(function calculateBonuses(params/*, hash*/) {
  let bonuses = null;
  if (params.food) {bonuses += `+${params.food} food`}
  if (params.production) {bonuses += `+${params.production} production`}
  if (params.gold) {bonuses += `+${params.gold} gold`}
  if (params.culture) {bonuses += `+${params.culture} culture`}
  return bonuses ? bonuses : '(no bonuses)';
});
