import { helper } from '@ember/component/helper';

export default helper(function monthString(params/*, hash*/) {
  if (params == 1) {return "Spring"}
  if (params == 2) {return "Summer"}
  if (params == 3) {return "Autumn"}
  if (params == 4) {return "Winter"}
});
