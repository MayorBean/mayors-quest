import { helper } from '@ember/component/helper';

export default helper(function pronoun(params/*, hash*/) {
  return params[0].gender === 'male' ? 'he' : 'she'
});
