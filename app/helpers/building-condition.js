import { helper } from '@ember/component/helper';

export function value(params) {
  if (params < 25) {return 'terrible'}
  if (params < 50 ) {return 'poor'}
  if (params < 75 ) {return 'okay'}
  if (params < 100) {return 'good'}
  else {return 'perfect'}
}

export default helper(value);
