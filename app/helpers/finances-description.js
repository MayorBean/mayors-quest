import { helper } from '@ember/component/helper';

export function value(params) {
  if (params < 0) {return 'bankrupt'}
  if (params < 1000 ) {return 'precarious'}
  if (params < 100000) {return 'okay'}
  if (params < 1000000) {return 'good'}
  return 'rich';
}

export default helper(value);
