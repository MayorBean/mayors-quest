import { helper } from '@ember/component/helper';

export default helper(function starRating(params/*, hash*/) {
  if (params < 40) {return '*'}
  if (params < 80) {return '**'}
  if (params < 120) {return '***'}
  if (params < 160) {return '****'}
  if (params <= 200) {return '*****'}
});
