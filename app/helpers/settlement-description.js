import { helper } from '@ember/component/helper';

export default helper(function settlementDescription(params/*, hash*/) {
  let output = params[1];
  let largest = Object.keys(output).reduce(function(largest,key){
    return (largest === undefined || output[key] > output[largest]) ? key : largest;
  });
  if (largest === 'productive') {return 'industrious'}
  if (largest === 'food') {return 'agricultural'}
  if (largest === 'gold') {return 'prosperous'}
  if (largest === 'culture') {return 'charming'}
});
