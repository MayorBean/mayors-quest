import { helper } from '@ember/component/helper';

export default helper(function hourString(params/*, hash*/) {
  if (params == 1) {return "morning"}
  if (params == 2) {return "afternoon"}
  if (params == 3) {return "evening"}
  if (params == 4) {return "night"}
});
