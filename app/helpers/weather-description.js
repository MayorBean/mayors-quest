import { helper } from '@ember/component/helper';

export default helper(function weatherDescription(params/*, hash*/) {
  let weather = params[0];
  let hour = params[1];
  if (weather == "clear") {
    if (hour < 3) {
      return "sunny"
    } else {
      return "clear"
    }
  }
  if (weather == "cloudy") {
    if (hour < 3) {
      return "dreary"
    } else {
      return "dark"
    }
  }
  if (weather == "rain") {return "rainy"}
  if (weather == "snow") {return "snowy"}
  if (weather == "thunderstorm") {return "stormy"}
  if (weather == "blizzard") {return "bitterly cold"}
  return weather;
});
