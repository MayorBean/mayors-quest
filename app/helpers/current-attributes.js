import { helper } from '@ember/component/helper';

export default helper(function currentAttributes(params/*, hash*/) {
  let character = params[0];
  let age = params[1]
  let currentAttributes = {
		agility: 1,
		balance: 1,
		dexterity: 1,
		reflexes: 1,
		speed: 1,
		stamina: 1,
		strength: 1,
		bravery: 1,
		charisma: 1,
		composure: 1,
		creativity: 1,
		intelligence: 1,
		leadership: 1,
		wisdom: 1
	};
  let mentalModifier = Math.min(Math.max((age/35), 0.8), 1.20);
  let physicalModifier = age <= 28 ? 1: 2 - mentalModifier;
	let potentialAttributes = {
		agility: Math.min((character.physicalAttributes.agility * physicalModifier), 20),
		balance: Math.min((character.physicalAttributes.balance * physicalModifier), 20),
		dexterity: Math.min((character.physicalAttributes.dexterity * physicalModifier), 20),
		reflexes: Math.min((character.physicalAttributes.reflexes * physicalModifier), 20),
		speed: Math.min((character.physicalAttributes.speed * physicalModifier), 20),
		stamina: Math.min((character.physicalAttributes.stamina * physicalModifier), 20),
		strength: Math.min((character.physicalAttributes.strength * physicalModifier), 20),
		bravery: Math.min((character.mentalAttributes.bravery * mentalModifier), 20),
		charisma: Math.min((character.mentalAttributes.charisma * mentalModifier), 20),
		composure: Math.min((character.mentalAttributes.composure * mentalModifier), 20),
		creativity: Math.min((character.mentalAttributes.creativity * mentalModifier), 20),
		intelligence: Math.min((character.mentalAttributes.intelligence * mentalModifier), 20),
		leadership: Math.min((character.mentalAttributes.leadership * mentalModifier), 20),
		wisdom: Math.min((character.mentalAttributes.wisdom * mentalModifier), 20)
	};
  for (let i = 0; i< character.class.majorAttributes.length; i++) {
    let key = character.class.majorAttributes[i];
    potentialAttributes[key] += character.level;
    if (potentialAttributes[key] > 20) {potentialAttributes[key] = 20}
	}
  for (let i = 0; i< character.class.minorAttributes.length; i++) {
    let key = character.class.minorAttributes[i];
    potentialAttributes[key] += (character.level / 2);
    if (potentialAttributes[key] > 20) {potentialAttributes[key] = 20}
  }
	for (let i = 0; i<character.ability + 50; i++) {
		let difference = {};
		let key;
		for (key in currentAttributes) {
			if (potentialAttributes[key] > currentAttributes[key]) {
				difference[key] = potentialAttributes[key] - currentAttributes[key];
			} else {
				difference[key] = 0;
			}
		}
		const biggest = Object.entries(difference)
			.reduce((biggest, current, ind) => {
				const parts = current
				return (!ind || parts[1] > biggest[1]) ? parts : biggest
		}, null);
		if (currentAttributes[biggest[0]] < potentialAttributes[biggest[0]]) {currentAttributes[biggest[0]]++}
	}
	return currentAttributes;
});
