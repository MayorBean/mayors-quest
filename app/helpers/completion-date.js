import { helper } from '@ember/component/helper';

export default helper(function completionDate([cost, production, date]/*, hash*/) {
  let days = Math.floor((cost / production) / 4);
  for (let i=0; i < days; i++) {
    date.day++
    if (date.day > 28) {
      date.day = 1;
      date.season++;
      if (date.season > 4) {
        date.season = 1;
        date.year++;
      }
    }
  }
  return date;
});
