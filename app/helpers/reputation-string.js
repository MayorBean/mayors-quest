import { helper } from '@ember/component/helper';

export function reputationString(reputation/*, hash*/) {
  if (reputation < 25) {return `unheard of`;}
  if (reputation < 50) {return `obscure`;}
  if (reputation < 75) {return `local`;}
  if (reputation < 100) {return `regional`;}
  if (reputation < 125) {return `well-known`;}
  if (reputation < 150) {return `renowned`;}
  if (reputation < 175) {return `famed`;}
  if (reputation <= 200) {return `legendary`;}
}

export default helper(reputationString);
