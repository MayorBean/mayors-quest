import { helper } from '@ember/component/helper';

export default helper(function calculateAge(params/*, hash*/) {
  let dateOfBirth = params[0];
  let currentDate = params[1];
  if (dateOfBirth.season < currentDate.season) {return currentDate.year - dateOfBirth.year}
  if (dateOfBirth.season > currentDate.season) {return currentDate.year - dateOfBirth.year - 1}
  if (dateOfBirth.season == currentDate.season && dateOfBirth.day < currentDate.day) {return currentDate.year - dateOfBirth.year}
  if (dateOfBirth.season == currentDate.season && dateOfBirth.day > currentDate.day) {return currentDate.year - dateOfBirth.year - 1}
  if (dateOfBirth.season == currentDate.season && dateOfBirth.day == currentDate.day) {return currentDate.year - dateOfBirth.year}
});
