import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('about');
  this.route('signup');
  this.route('load');
  this.route('home', function() {
    this.route('cutscenes', function() {
      this.route('first-visit');
      this.route('tutorial-navigation');
    });
  });
  this.route('inbox', function() {
    this.route('welcome', {path: '/welcome/:id'});
    this.route('construction-approved', {path: '/construction-approved/:id'});
    this.route('construction-rejected', {path: '/construction-rejected/:id'});
    this.route('repairing', {path: '/repairing/:id'});
    this.route('construction-complete', {path: '/construction-complete/:id'});
    this.route('repairs-complete', {path: '/repairs-complete/:id'});
    this.route('introduction', {path: '/introduction/:id'});
  });
  this.route('journal');
  this.route('county', {path: '/county/:id'});
  this.route('character', {path: '/character/:id'});
  this.route('region', {path: '/region/:id'});
  this.route('tavern', function() {
    this.route('cutscenes', function() {
      this.route('first-visit');
    });
  });
  this.route('barracks', function() {
    this.route('cutscenes', function() {
      this.route('first-visit');
    });
  });
  this.route('item');
  this.route('profile');
  this.route('hall', function() {
    this.route('registry');
    this.route('treasury');
    this.route('planning');
    this.route('reception');
    this.route('cutscenes', function() {
      this.route('first-visit');
    });
  });
  this.route('rest');
  this.route('calendar');
  this.route('rest-loading');
  this.route('settlement', {path: '/settlement/:id'}, function() {
    this.route('overview', {path: '/overview'});
    this.route('heroes', {path: '/heroes'});
  });
});

export default Router;
