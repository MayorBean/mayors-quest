import Model from 'ember-data/model';
import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default Model.extend({
  name: attr('String'),
  players: attr(),
  date: attr(),
  kingdom: attr('String'),
  monarch: belongsTo('character'),
  capital: belongsTo('town'),
  regions: hasMany('region', {inverse: 'world'}),
  counties: hasMany('county', {inverse: 'world'}),
  towns: hasMany('town', {inverse: 'world'}),
  settings: attr(),
  createdAt: attr('String'),
  updatedAt: attr('String')
});
