import Model from 'ember-data/model';
import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default Model.extend({
  name:             attr('String'),
  status:           attr('String'),
  world:            belongsTo('world', {inverse: 'towns'}),
  region:           belongsTo('region', {inverse: 'towns'}),
  county:           belongsTo('county', {inverse: 'towns'}),
  founded:          attr('Number'),
  population:       attr('Number'),
  mayor:            belongsTo('character', {inverse: null}),
  player:           belongsTo('player'),
  reputation:       attr('Number'),
  climate:          attr('String'),
  biome:            attr('String'),
  landform:         attr('String'),
  finances:         attr(),
  buildings:        hasMany('building', {inverse: null}),
  project:          belongsTo('building', {inverse: null}),
  tiles:            attr()
});
