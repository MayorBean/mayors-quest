import DS from 'ember-data';
import MF from 'ember-data-model-fragments';

const { attr } = DS;

export default MF.Fragment.extend({
  name: attr('String'),
  author: attr('String'),
  size: attr('Number'),
  names: attr(),
  classes: attr(),
  professions: attr(),
  climates: attr()
});
