import DS from 'ember-data';
import MF from 'ember-data-model-fragments';

const { attr } = DS;

export default MF.Fragment.extend({
  hour: attr('Number'),
  day: attr('Number'),
  season: attr('Number'),
  year: attr('Number')
});
