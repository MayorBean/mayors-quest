import DS from 'ember-data';

const { attr } = DS;

export default DS.Model.extend({
  name: attr('String'),
  type: attr('String'),
  value: attr('Number')
});
