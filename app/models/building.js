import DS from 'ember-data';

const { attr, belongsTo } = DS;

export default DS.Model.extend({
  name:           attr('String'),
  type:           attr('String'),
  level:          attr('Number'),
  health:         attr('Number'),
  maxHealth:      attr('Number'),
  world:          belongsTo('world', {inverse: null}),
  town:           belongsTo('town', {inverse: null}),
  founded:        attr('Number'),
  owner:          belongsTo('character', {inverse: 'realEstate'}),
  reputation:     attr('Number'),
  cutscene:       attr(),
  food:           attr('Number'),
  production:     attr('Number'),
  gold:           attr('Number'),
  culture:        attr('Number'),
  cost:           attr('Number'),
  progress:       attr('Number'),
  needsTile:      attr('Boolean'),
  tile:           attr('Number'),
  description:    attr('String')
});
