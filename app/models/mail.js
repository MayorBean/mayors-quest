import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default DS.Model.extend({
  world:       belongsTo('world'),
  sender:      belongsTo('character'),
  recipient:   belongsTo('player'),
  date:        attr(),
  town:        belongsTo('town'),
  subject:     attr('String'),
  content:     attr('String'),
  unread:      attr('Boolean'),
  responses:   attr(),
  buildings:   attr(),
  characters:  hasMany('character'),
  createdAt:   attr('String'),
});
