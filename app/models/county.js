import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default DS.Model.extend({
  name: attr('string'),
  region: belongsTo('region', {inverse: 'counties'}),
  biome: attr(),
  world: belongsTo('world'),
  towns: hasMany('town', {inverse: 'county'}),
  capital: belongsTo('town'),
});
