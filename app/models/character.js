import Model from 'ember-data/model';
import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default Model.extend({
  firstName:           attr('String'),
  lastName:            attr('String'),
  gender:              attr('String'),
  race:                attr('String'),
  world:               belongsTo('world', {inverse: null}),
  birthplace:          belongsTo('town', {inverse: null}),
  home:                belongsTo('town', {inverse: null}),
  location:            belongsTo('town', {inverse: null}),
  profession:          attr('String'),
  class:               attr(),
  level:               attr('Number'),
  trait:               attr('String'),
  dateOfBirth:         attr(),
  experience:          attr('Number'),
  reputation:          attr('Number'),
  potential:           attr('Number'),
  ability:             attr('Number'),
  morale:              attr('String'),
  status:              attr('String'),
  mentalAttributes:    attr(),
  physicalAttributes:  attr(),
  hiddenAttributes:    attr(),
  deformities:         attr(),
  playersMet:          hasMany('player', {inverse: null}),
  realEstate:          belongsTo('building', {inverse: 'owner'}),
//inventory:           hasMany('item'),
//armour:
//weapon:
//stats:
});
