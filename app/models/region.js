import DS from 'ember-data';

const { attr, belongsTo, hasMany } = DS;

export default DS.Model.extend({
  name: attr('string'),
  world: belongsTo('world', {inverse: 'regions'}),
  climate: attr(),
  weather: attr('string'),
  capital: belongsTo('town', {inverse: 'region'}),
  towns: hasMany('town', {inverse: 'region'}),
  counties: hasMany('county', {inverse: 'region'})
});
