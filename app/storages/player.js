import StorageObject from 'ember-local-storage/session/object';

const Storage = StorageObject.extend();

export default Storage;
