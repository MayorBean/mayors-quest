import DS from "ember-data";
import ENV from '../config/environment';

export default DS.RESTAdapter.extend({
  authorizer: "authorizer:application",
  namespace: ENV.namespace
});
