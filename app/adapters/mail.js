import DS from 'ember-data';
import { singularize } from 'ember-inflector';
import ENV from '../config/environment';

export default DS.RESTAdapter.extend({
  host: ENV.host,
  namespace: ENV.namespace,
  pathForType: function(modelName) {
    return singularize(modelName);
  }
});
