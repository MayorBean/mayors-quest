import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { storageFor } from "ember-local-storage";
import { inject as controller} from '@ember/controller';

export default Controller.extend({
  player: storageFor('player'),
  application: controller('application'),
  selected: null,
  planBuilding: false,
  repairable:computed('selected', function() {
    return this.get('selected.health') < this.get('selected.maxHealth') ? true : false
  }),
  demolishable:computed('selected', function() {
    if (this.get('selected.type') === 'hall' || 'barracks' || 'residence' || 'tavern') {return false}
    else {return true}
  }),
  actions: {
    newBuilding() {
      this.toggleProperty('planBuilding');
    },
    select(building) {
      building != this.set('selected') ? this.set('selected', building) : this.refresh('selected');
    },
    repair(building, settlement, chancellor) {
      let date = this.player.get('date');
      settlement.set('project', building);
      settlement.save();
      let mail = this.store.createRecord('mail', {
        world:       settlement.world,
        sender:      chancellor,
        recipient:   settlement.player,
        date:        {hour: date.hour, day: date.day, season: date.season, year: date.year},
        town:        settlement,
        subject:     `Plans to repair ${building.name} approved`,
        content:     `repairing`,
        buildings:   [building.get('id')],
      });
      mail.save();
      this.player.incrementProperty('mail');
      this.set('application.sidebar', true);
    },
    demolish(building) {
      this.set('selected', building);
      this.set('usedRequest', true);
    }
  }
});
