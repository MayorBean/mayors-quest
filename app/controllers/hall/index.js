import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  project:computed('home.project', function() {
    let project = this.get('home.project');
    if (!project.content) {return 'none'}
    else {
      if (project.get('founded')) {
        return `repairing ${project.get('name')}`
      } else {
        return `building a ${project.get('type')}`
      }
    }
  })
});
