import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  session: service('session'),
  actions: {
    save(username, password, confirmPassword){
      if (!username) {return this.set('errorMessage', {message: 'username cannot be blank'})}
      if (password !== confirmPassword) {return this.set('errorMessage', {message: 'passwords do not match.'})}
      let attrs = { username, password };
      let player = this.store.createRecord('player', attrs)
      player.save().catch((error) => {
        this.set('errorMessage', error)
      })
      .then(() => {
        this.get('session').authenticate('authenticator:oauth2', username, password)
        .catch((error) => {
          this.set('loginError', 'Signup failed: ' + JSON.stringify(error.message));
        })
      })
    }
  }
});
