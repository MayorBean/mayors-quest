import Controller from '@ember/controller';
import { storageFor } from 'ember-local-storage';

export default Controller.extend({
  player: storageFor("player"),
  sidebar: false,
  actions: {
    invalidateSession() {
      if (this.get('player')) {
        this.get('player').clear();
      };
      this.get('session').invalidate();
      this.refresh();
    },
    toggleMenu() {
      this.toggleProperty('sidebar');
    },
    openMenu() {
      this.set('sidebar', true);
    }
  }
})
