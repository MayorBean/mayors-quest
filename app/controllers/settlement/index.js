import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { storageFor } from "ember-local-storage";

export default Controller.extend({
  player: storageFor('player'),
  terrain:computed('settlement.tiles.0.terrain.name', function() {
    return this.get('settlement.tiles.0.terrain.name');
  }),
  biome:computed('settlement.county.biome.name', function() {
    return this.get('settlement.county.biome.name');
  }),
  preposition:computed('settlement', function() {
    if (this.get('biome') === 'coast' || this.get('biome') === 'plains') {return ` on the`}
    else {return ` in the`}
  }),
  situ:computed('settlement.tiles.0.terrain.name', function() {
    if (this.get('terrain') == this.get('biome')) {return `in the heart of the`}
    if (this.get('terrain') == 'hills') {return `on a hill` + this.get('preposition')}
    if (this.get('terrain') == 'river') {return 'on a river' + this.get('preposition')}
    if (this.get('terrain') == 'oasis') {return `around a tranquil oasis` + this.get('preposition')}
    if (this.get('terrain') == 'lake') {return `around a lake` + this.get('preposition')}
    if (this.get('terrain') == 'coast') {return this.get('preposition')}
    if (this.get('terrain') == 'mountains') {return `atop a mountain` + this.get('preposition')}
    if (this.get('terrain') == 'jungle') {return `and lies deep within a thick jungle` + this.get('preposition')}
    if (this.get('terrain') == 'plains') {return this.get('preposition')}
    if (this.get('terrain') == 'forest') {return `and lies deep within a great forest` + this.get('preposition')}
  })
});
