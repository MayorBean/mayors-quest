import Controller from '@ember/controller';

export default Controller.extend({
  sortProperties: ['reputation:desc'],
  sortedTowns: Ember.computed.sort('towns', 'sortProperties')
})
