import Controller from '@ember/controller';

export default Controller.extend({
  incrementTime(world) {
    let hour = world.date.hour;
    let day = world.date.day;
    let season = world.date.season;
    let year = world.date.year;
    if (hour < 4) {hour++}
    else {
      hour = 1;
      if (day < 28) {day++}
      else {
        day = 1;
        if (season < 4) {season++}
        else {
          season = 1;
          year++
        }
      }
    }
    world.set('date.hour', hour);
    world.set('date.day', day);
    world.set('date.season', season);
    world.set('date.year', year);
    return world;
  }
});
