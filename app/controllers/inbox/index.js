import Controller from '@ember/controller';
import { storageFor } from "ember-local-storage";
import { computed } from "@ember/object";

export default Controller.extend({
  player: storageFor('player'),
  sortProperties: ['createdAt:desc'],
  sortedMail: computed.sort('mail', 'sortProperties'),
  actions: {
    read(message){
      if (message.get('unread')) {
        this.decrementProperty('player.mail');
        message.set('unread', false);
        message.save();
      }
    }
  }
});
