import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  view:computed('view', function() {
    return this.get('view');
  }),
  navItems: ['overview', 'heroes']
});
