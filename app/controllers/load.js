import Controller from "@ember/controller";
import { computed } from "@ember/object";
import { compare } from "@ember/utils";
import { inject as service } from "@ember/service";
import { capitalize } from "@ember/string";
import { storageFor } from "ember-local-storage";

export default Controller.extend({
  session: service("session"),
  player: storageFor("player"),
  deleting: false,
  status: null,
  error: false,
  store: service(),
  loading: false,
  sortProperties: ['updatedAt'],
  sortAscending: false,
  sortedWorlds: computed("worlds", "sortProperties", "sortAscending", function(){
    return this.get("worlds").toArray().sort((a, b)=>{
      let sortProperty = this.get("sortProperties")[0];
      if (this.get("sortAscending")){
        return compare(a.get(sortProperty), b.get(sortProperty));
      } else {
        return compare(b.get(sortProperty), a.get(sortProperty));
      }
    })
  }),
  displayError(err) {
    this.set("status", err);
    this.toggleProperty("error");
  },
  delete(world) {
    let confirm = window.confirm("Are you sure you want to delete world: " + world.name);
    if (confirm) {
      this.store.findRecord("world", world.id, {reload: true}).then((post) => {
        post.deleteRecord();
        post.get("isDeleted");
        post.save().then(() => {this.transitionToRoute("/")});
      });
    }
  },
  async setPlayer(world) {
    let playerId = this.get("session.data.authenticated._id");
    let home = await this.store.queryRecord("town", {player: playerId, world: world.get('id')});
    let mail = await this.store.query("mail", {world: world.get('id'), recipient: playerId, unread: true});
    this.player.set("worldId", world.get("id"));
    this.player.set("mail", mail.length);
    this.player.set("date", world.date);
    this.player.set("settings", world.settings);
    this.player.set("town", home);
    this.player.set("townId", home.get("id"));
    this.player.set("isLoaded", true);
    this.player.set("playerId", playerId);
    if (this.get("session.player.username")) {
      this.player.set("username", capitalize(this.get("session.player.username")));
    };
    this.transitionToRoute("home")
  },
  actions: {
    async create() {
      const settings = this.get("settings");
      const name = prompt("Enter a world name", capitalize(this.get("session.player.username")) + "'s World");
      // So much of this should be done server-side to reduce the insane number of callbacks
      if (name != null) {
        this.toggleProperty("loading");
        this.set('status', "Generating world...");
        let world = this.store.createRecord('world', {
          name: name,
          date: {hour: 1, day: 1, season: 1, year: 753},
          settings: settings,
          players: this.get('session.player.id')
        })
        await world.save();
        this.send('load', world);
      }
    },
    async load(world) {
      if (this.get("deleting", true)) {
        this.delete(world);
        this.set("deleting", false);
      } else {
        this.setPlayer(world);
      }
    },
    delete() {
      this.toggleProperty("deleting");
    },
  }
});
