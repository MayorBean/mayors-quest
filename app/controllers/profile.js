import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { inject as controller } from '@ember/controller';

export default Controller.extend({
  name: service(),
  character: controller(),
  profile: null,
  actions:{
    create (settings) {
      this.set('profile', this.name.tavern(settings.names));
    }
  }
});
