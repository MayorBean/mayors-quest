import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: '_id',
  attrs: {
    region: {serialize: 'ids', deserialize: 'ids'},
    county: {serialize: 'ids', deserialize: 'ids'},
    project: {serialize: 'ids', deserialize: 'ids'},
    mayor: {serialize: 'ids', deserialize: 'ids'},
    buildings: {serialize: 'ids', deserialize: 'ids'}
  },
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {town: payload};
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
