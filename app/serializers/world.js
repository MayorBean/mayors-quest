import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: '_id',
  attrs: {
    capital: {serialize: 'ids', deserialize: 'ids'},
    monarch: {serialize: 'ids', deserialize: 'ids'}
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = { worlds: payload };
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
