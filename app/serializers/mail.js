import DS from 'ember-data';
import { singularize } from 'ember-inflector';

export default DS.RESTSerializer.extend({
  primaryKey: '_id',
  payloadKeyFromModelName(modelName) {
    return singularize(modelName); //This ensures that Ember understands the plural of mail is mail.
  },
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {mail: payload};
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
