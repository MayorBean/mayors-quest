import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  primaryKey: '_id',
  attrs: {
    owner: {serialize: 'ids', deserialize: 'ids'}
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {building: payload};
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
