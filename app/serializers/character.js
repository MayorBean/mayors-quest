import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  primaryKey: '_id',
  attrs: {
    birthplace: {serialize: 'ids', deserialize: 'ids'},
    location: {serialize: 'ids', deserialize: 'ids'},
    home: {serialize: 'ids', deserialize: 'ids'}
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {character: payload};
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
