import DS from 'ember-data';

export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: '_id',
  attrs: {
    region: {serialize: 'ids', deserialize: 'ids'},
    capital: {serialize: 'ids', deserialize: 'ids'}
  },

  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    payload = {counties: payload};
    return this._super(store, primaryModelClass, payload, id, requestType);
  }
});
