import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model: async function(params) {
    let region = await this.store.findRecord('region', params.id);
    let towns = await this.store.query('town', {region: region.get('id')});
    towns = towns.filter(town => town.status != 'village');
    return hash({
      region: region,
      counties: this.store.findRecord('region', region.get('id')),
      towns: towns,
      capital: this.store.findRecord('town', region.capital.get('id'))
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
