import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import { hash } from 'rsvp';

export default Route.extend({
  player: storageFor("player"),
  model: async function() {
    return hash({
      player: this.get('player'),
      settlement: this.store.findRecord('town', this.get('player.townId')),
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  }
});
