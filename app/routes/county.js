import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model: async function(params) {
    let county = await this.store.findRecord('county', params.id);
    return hash({
      county: county,
      region: this.store.findRecord('region', county.region.get('id')),
      towns: this.store.query('town', {county: county.get('id')}),
      capital: this.store.findRecord('town', county.capital.get('id'))
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
