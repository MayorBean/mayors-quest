import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Route.extend(ApplicationRouteMixin,{
  session: service(),
  player: storageFor('player'),
  model: async function() {
    if (this.player.get('townId')) {
      return hash({
        mail: this.store.query('mail', {recipient: this.player.get('town.player.id'), unread: true, world: this.player.get('town.world')})
      })
    }
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
})
