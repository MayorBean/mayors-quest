import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { storageFor } from 'ember-local-storage';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin, {
  player: storageFor("player"),
  model: async function() {
    let settlement = await this.store.findRecord('town', this.player.get('townId'));
    let project = await settlement.get('project') ? this.store.findRecord('building', settlement.project.get('id')) : null;
    let residence = await this.store.queryRecord('building', {town: settlement.get('id'), type: 'residence'});
    return hash({
      settlement: settlement,
      date: this.player.get('date'),
      county: this.store.findRecord('county', settlement.county.get('id')),
      region: this.store.findRecord('region', settlement.region.get('id')),
      building: residence,
      project: project,
      mail: this.store.query('mail', {recipient: settlement.player.get('id'), unread: true, world: settlement.world.get('id')})
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
