import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import { hash } from 'rsvp';

export default Route.extend({
  player: storageFor("player"),
  model: async function() {
    let home = await this.store.findRecord('town', this.player.get('townId'));
    let growth = [];
    home.finances.forEach(statement => {
      growth.unshift(statement.balance);
    });
    return hash({
      hall: this.store.queryRecord('building', {town: this.player.get('townId'), type: 'hall'}),
      home: home,
      growth: growth
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  }
});
