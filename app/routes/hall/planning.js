import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import { hash } from 'rsvp';

export default Route.extend({
  player: storageFor("player"),
  model: async function() {
    let hall = await this.store.queryRecord('building', {town: this.player.get('townId'), type: 'hall'});
    let buildable = [];
    let buildings = await this.store.query('building', {town: this.player.get('townId')});
    let availableBuildings = this.player.get('settings.buildings').filter(building => building.needsTile === false);
    availableBuildings.forEach(availableBuilding => {
      let alreadyBuilt = false;
      buildings.forEach(building => {
        if (building.type == availableBuilding.formType) {alreadyBuilt = true}
      })
      if (!alreadyBuilt) {buildable.push(availableBuilding)}
    });
    return hash({
      hall: hall,
      home: this.store.findRecord('town', this.player.get('townId')),
      buildings: buildings,
      availableBuildings: buildable,
      chancellor: this.store.findRecord('character', hall.get('owner.id'))
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  }
});
