import Route from '@ember/routing/route';

export default Route.extend({
  setupController(controller, models) {
    controller.setProperties(models);
  },
});
