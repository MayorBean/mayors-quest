import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import RSVP from 'rsvp';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model() {
    return RSVP.hash({
      heroes: this.store.query('character', {home: this.paramsFor('settlement').id, profession: 'hero'}),
      date: this.get("player.date"),
    })
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
