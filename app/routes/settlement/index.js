import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model: async function() {
    let settlement = await this.store.findRecord('town', this.paramsFor('settlement').id);
    return hash({
      settlement: settlement,
      player: this.get("player"),
      mayor: this.store.queryRecord('character', {home: settlement.get('id'), profession: 'mayor'}),
      county: this.store.findRecord('county', settlement.county.get('id')),
      region: this.store.findRecord('region', settlement.region.get('id'))
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
