import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor('player'),
  model: async function() {
    let building = await this.store.queryRecord('building', {town: this.get('player.townId'), type: 'barracks'});
    return hash({
      heroes: this.store.query('character', {home: this.get('player.townId'), profession: 'hero'}),
      owner: this.store.findRecord('character', building.owner.get('id')),
      building: building,
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
