import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  session: service(),

  model: function(params) {
    return hash({
      world: this.store.findRecord('world', params.id),
      player: this.store.query('player', {players: this.session.data.authenticated._id})
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
});
