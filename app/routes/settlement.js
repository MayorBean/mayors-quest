import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model(params) {
    return this.store.findRecord('town', params.id);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
