import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import jQuery from 'jquery';

export default Route.extend(AuthenticatedRouteMixin,{
  model() {
    return jQuery.getJSON({
      url: '../data/default.json',
      type: 'get',
      dataType: 'json'
    });
  },
});
