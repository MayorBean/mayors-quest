import Route from '@ember/routing/route';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Route.extend(UnauthenticatedRouteMixin, {
  session: service(),
  player: storageFor("player"),
  redirect() {
    let player = this.get('player');
    if (this.get('session.isAuthenticated')) {
      if (player.get('isLoaded')) {
        this.transitionTo('home');
      } else {
        this.transitionTo('load');
      }
    } else {
      this.transitionTo('about');
    }
  }
});
