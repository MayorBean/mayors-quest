import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor("player"),
  model: async function() {
    let hall = await this.store.queryRecord('building', {town: this.player.get('townId'), type: 'hall'})
    return hash({
      hall: hall,
      player: this.player,
      home: this.store.findRecord('town', this.player.get('townId')),
      owner: this.store.findRecord('character', hall.get('owner.id'))
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
