import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin, {
  session: service(),
  player: storageFor("player"),
  model: async function(params) {
    let mail = await this.store.findRecord('mail', params.id);
    let chancellor = await this.store.findRecord('character', mail.sender.get('id'));
    let settlement = await this.store.findRecord('town', chancellor.home.get('id'));
    return hash({
      mail: mail,
      chancellor: chancellor,
      settlement: settlement,
      username: this.player.get('username')
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
