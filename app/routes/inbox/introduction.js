import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { storageFor } from 'ember-local-storage';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin, {
  player: storageFor("player"),
  model: async function(params) {
    let mail = await this.store.findRecord('mail', params.id);
    let sender = await this.store.findRecord('character', mail.sender.get('id'));
    let settlement = await this.store.findRecord('town', sender.home.get('id'));
    let building = await this.store.findRecord('building', mail.buildings[0]);
    let blueprint = this.player.get('settings.buildings').find(blueprint => blueprint.formType === building.type);
    return hash({
      mail: mail,
      sender: sender,
      settlement: settlement,
      username: this.player.get('username'),
      building: building,
      profession: blueprint.profession
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
