import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin, {
  session: service(),
  player: storageFor("player"),
  model: async function(params) {
    let mail = await this.store.findRecord('mail', params.id);
    let chancellor = await this.store.findRecord('character', mail.sender.get('id'));
    let settlement = await this.store.findRecord('town', chancellor.home.get('id'));
    let project = await this.store.findRecord('building', mail.buildings[0]);
    let projectDescription = this.player.get('settings.buildings').filter(building => building.formType === project.type);
    return hash({
      mail: mail,
      chancellor: chancellor,
      settlement: settlement,
      username: this.player.get('username'),
      project: project,
      profession: projectDescription[0].profession
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
