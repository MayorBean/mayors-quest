import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { storageFor } from 'ember-local-storage';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor('player'),
  model() {
    return this.player.get('date');
  }
});
