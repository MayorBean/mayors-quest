import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';

export default Route.extend(AuthenticatedRouteMixin,{
  player: storageFor('player'),
  model: function(params) {
    return hash({
      player: this.player,
      character: this.store.find('character', params.id)
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (!this.get('player.isLoaded')) {this.transitionTo('load');}
  }
});
