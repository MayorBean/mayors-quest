import Route from '@ember/routing/route';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { hash } from 'rsvp';
import jQuery from 'jquery';

export default Route.extend(AuthenticatedRouteMixin,{
  session: service(),
  player: storageFor("player"),
  model: function() {
    return hash({
      worlds: this.store.query('world', {players: this.session.data.authenticated._id}),
      settings: jQuery.getJSON({
        url: '../data/default.json',
        type: 'get',
        dataType: 'json'
      })
    })
  },
  setupController(controller, models) {
    controller.setProperties(models);
  },
  redirect() {
    if (this.get('player.isLoaded')) {this.transitionTo('home');}
  }
});
