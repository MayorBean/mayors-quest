import Route from '@ember/routing/route';
import { storageFor } from "ember-local-storage";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  player: storageFor('player'),
  async model() {
    let world = await this.store.findRecord('world', this.player.get('worldId'));
    this.controllerFor('rest').incrementTime(world);
    await world.save()
    await this.controllerFor('load').setPlayer(world);
    this.transitionTo("home");
  },
  actions: {
    loading(transition) {
      let controller = this.controllerFor('rest');
      controller.set('currentlyLoading', true);
      transition.promise.finally(function() {
          controller.set('currentlyLoading', false);
      });

      return true; // allows the loading template to be shown
    }
  }
});
