import Component from '@ember/component';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';

export default Component.extend({
  player: storageFor('player'),
  session: service('session'),
  actions:{
    toggleMenu() {
      this.toggleProperty('sidebar');
    }
  }
});
