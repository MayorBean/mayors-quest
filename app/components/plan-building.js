import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from "@ember/service";
import { capitalize } from "@ember/string";
import { storageFor } from "ember-local-storage";

export default Component.extend({
  selected: false,
  session: service('session'),
  store: service('store'),
  player: storageFor('player'),
  bonuses:computed('selected', function() {
    let bonuses = ``;
    if (this.selected.food) {bonuses += `+${this.selected.food} food`}
    if (this.selected.production) {bonuses += `+${this.selected.production} production`}
    if (this.selected.gold) {bonuses += `+${this.selected.gold} gold`}
    if (this.selected.culture) {bonuses += `+${this.selected.culture} culture`}
    return bonuses;
  }),
  actions: {
    close() {
      this.toggleProperty('active');
    },
    select(building) {
      this.set('selected', building);
    },
    async build(building, settlement, chancellor) {
      if (settlement.finances[0].balance > 0) {
        let project = this.store.createRecord('building', {
          name:           `The ${capitalize(building.formType)}`,
          type:           building.formType,
          level:          1,
          health:         100,
          maxHealth:      100,
          world:          settlement.world,
          town:           settlement,
          founded:        null,
          owner:          null,
          reputation:     this.player.get('town.reputation'),
          cutscene:       null,
          food:           building.food,
          production:     building.production,
          gold:           building.gold,
          culture:        building.culture,
          cost:           building.cost,
          progress:       0,
          needsTile:      building.needsTile,
          tile:           0,
          description:    building.description,
          constructing:   true
        });
        await project.save();
        settlement.set('project', project);
        settlement.buildings.pushObject(project);
        await settlement.save();
        let date = this.player.get('date');
        let mail = this.store.createRecord('mail', {
          world:       settlement.world,
          sender:      chancellor,
          recipient:   settlement.player,
          date:        {hour: date.hour, day: date.day, season: date.season, year: date.year},
          town:        settlement,
          buildings:   [project.get('id')],
          subject:     `Plans for a ${building.formType} approved`,
          content:     `construction-approved`
        });
        mail.save();
      } else {
        let date = this.player.get('date');
        let mail = this.store.createRecord('mail', {
          world:       settlement.world,
          sender:      chancellor,
          recipient:   settlement.player,
          date:        {hour: date.hour, day: date.day, season: date.season, year: date.year},
          town:        settlement,
          subject:     `Plans for a ${building.formType} rejected`,
          content:     `construction-rejected`
        });
        mail.save();
      }
      this.player.incrementProperty('mail');
      this.set('sidebar', true);
      this.toggleProperty('active');
    }
  }
});
