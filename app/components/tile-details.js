import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from "@ember/service";

export default Component.extend({
  store: service('store'),
  tile:computed('selected', function() {
    return this.tiles[this.selected];
  }),
  food:computed('selected', function() {
    let food = this.tile.resource ? (this.tile.resource.food + this.tile.terrain.food) : this.tile.terrain.food;
    if (this.tile.building) {food += this.get('improvement.food')}
    return food;
  }),
  production:computed('selected', function() {
    let production = this.tile.resource ? (this.tile.resource.production + this.tile.terrain.production) : this.tile.terrain.production;
    if (this.tile.building) {production += this.get('improvement.production')}
    return production;
  }),
  gold:computed('selected', function() {
    let gold = this.tile.resource ? (this.tile.resource.gold += this.tile.terrain.gold) : this.tile.terrain.gold;
    if (this.tile.building) {gold += this.get('improvement.gold')}
    return gold;
  }),
  culture:computed('selected', function() {
    let culture = this.tile.resource ? (this.tile.resource.culture += this.tile.terrain.culture) : this.tile.terrain.culture;
    if (this.tile.building) {culture + this.get('improvement.culture')}
    return culture;
  }),
  resource:computed('selected', function() {
    return this.tile.resource ? this.tile.resource.name : 'none';
  }),
  terrain:computed('selected', function() {
    return this.tile.terrain.name ? this.tile.terrain.name : 'none';
  }),
  owner:computed('selected', function() {
    return this.tile.owner ? this.store.findRecord('character', this.tile.owner) : 'none';
  }),
  improvement:computed(function() {
    return this.tile.building ? this.store.peekRecord('building', this.tile.building) : 'none';
  })
});
