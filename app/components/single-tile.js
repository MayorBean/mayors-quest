import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from "@ember/service";

export default Component.extend({
  store: service('store'),
  selected:computed('building', function() {
    return this.store.findRecord('building', this.building)
  }),
  label:computed(function() {
    if (this.building) {return this.building.name}
    if (this.tile.resource) {return this.tile.resource.name}
    else {return this.tile.terrain.name}
  })
});
