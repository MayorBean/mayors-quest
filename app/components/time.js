import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  dayString:computed('date.day', function(){
    if (this.get('date.day') == 1 || this.get('date.day') == 8  || this.get('date.day') == 15 || this.get('date.day') == 22) {return 'Monday'}  // 'Mani'
    if (this.get('date.day') == 2 || this.get('date.day') == 9  || this.get('date.day') == 16 || this.get('date.day') == 23) {return 'Tuesday'}  // 'Tyr'
    if (this.get('date.day') == 3 || this.get('date.day') == 10 || this.get('date.day') == 17 || this.get('date.day') == 24) {return 'Wednesday'}  // 'Wodan'
    if (this.get('date.day') == 4 || this.get('date.day') == 11 || this.get('date.day') == 18 || this.get('date.day') == 25) {return 'Thursday'}  // 'Thor'
    if (this.get('date.day') == 5 || this.get('date.day') == 12 || this.get('date.day') == 19 || this.get('date.day') == 26) {return 'Friday'}  // 'Frigg'
    if (this.get('date.day') == 6 || this.get('date.day') == 13 || this.get('date.day') == 20 || this.get('date.day') == 27) {return 'Saturday'}  // 'Sattur'
    if (this.get('date.day') == 7 || this.get('date.day') == 14 || this.get('date.day') == 21 || this.get('date.day') == 28) {return 'Sunday'} // 'Sunna'
  }),
  seasonString:computed('date.season', function() {
    switch (this.get('date.season')) {
      case 1: return 'Spring'; //'Imbolc'
      case 2: return 'Summer'; //'Beltane'
      case 3: return 'Autumn'; //'Lammas'
      case 4: return 'Winter'; //'Samhain'
  }}),
  dateOrdinal:computed('date.day', function() {
    if (this.get('date.day') > 3 && this.get('date.day') < 21) return 'th';
    switch (this.get('date.day') % 10) {
      case 1:  return "st";
      case 2:  return "nd";
      case 3:  return "rd";
      default: return "th";
    }
  }),
})
