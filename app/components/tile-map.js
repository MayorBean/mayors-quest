import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  actions:{
    setTile(id) {
      id !=this.selected ? this.set('selected', id): this.set('selected', null)
    }
  }
});
