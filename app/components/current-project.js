import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from "@ember/service";

export default Component.extend({
  store: service('store'),
  activity:computed(function() {
    return this.project.get('founded') ? 'repairing' : 'building a'
  }),
  building:computed(function() {
    this.store.findRecord('building', this.project.get('id'));
  })
});
