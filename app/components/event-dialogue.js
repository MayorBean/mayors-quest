import Component from '@ember/component';
import { storageFor } from "ember-local-storage";
import { computed } from '@ember/object';

export default Component.extend({
  player: storageFor("player"),
  firstVisitHome:computed(function() {
    if (this.building.cutscene == "Your adventure begins") {return true;}
  }),
  firstVisitHall:computed(function() {
    if (this.building.cutscene == "First day at the office") {return true;}
  }),
  firstVisitTavern:computed(function() {
    if (this.building.cutscene == "A journey to the pub") {return true;}
  }),
  firstVisitBarracks:computed(function() {
    if (this.building.cutscene == "Meeting the local heroes") {return true;}
  }),
  actions:{
    close(building) {
      let town = building.get('town');
      building.set('cutscene', undefined);
      building.save();
    }
  }
});
