import Component from '@ember/component';
import { storageFor } from 'ember-local-storage';
import { inject as service } from '@ember/service';

export default Component.extend({
  session: service('session'),
  player: storageFor('player'),
  actions:{
    invalidateSession() {
      this.get('player').clear();
      this.get('session').invalidate();
    },
    toggleMenu() {
      this.toggleProperty('sidebar');
    }
  }
});
