import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  session: service('session'),
  incrementTime(hour, day, season, year) {
    if (hour < 4) {hour++; this.save(hour, day, season, year);}
    else {
      hour = 1;
      if (day < 28) {day++; this.save(hour, day, season, year);}
      else {
        day = 1;
        if (season < 4) {season++; this.save(hour, day, season, year)}
        else {
          season = 1;
          year++; this.save(hour, day, season, year)
        }
      }
    }
  }
});
