import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  session: service('session'),
  actions: {
    authenticate() {
      const { username, password } = this.getProperties('username', 'password');
      this.get('session').authenticate('authenticator:oauth2', username, password)
      .catch((err) => {
        if (err) {this.set('loginError', 'Login failed: ' + JSON.stringify(err.message))}
        else {this.set('loginError', 'Unable to connect to server...')}
      })
    }
  }
});
