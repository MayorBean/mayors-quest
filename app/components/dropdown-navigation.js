import Component from '@ember/component';

export default Component.extend({
  show: false,
  selected: null,
  actions: {
    toggleDropdown() {
      this.toggleProperty('show');
    },
    select(item) {
      this.set('selected', item);
      this.toggleProperty('show');
    }
  }
});
