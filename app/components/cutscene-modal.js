import Component from '@ember/component';

export default Component.extend({
  actions:{
    close(building) {
      let town = building.get('town');
      building.set('cutscene', undefined);
      building.save();
    }
  }
});
