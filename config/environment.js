'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'mayors-quest',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };
  ENV['ember-simple-auth'] = {
    rootURL:                     '',
    authenticationRoute:         'login',
    routeAfterAuthentication:    'load',
    routeIfAlreadyAuthenticated: 'load'
  }

  if (environment === 'development') {
    ENV.host = 'http://localhost:3000';
    ENV.namespace = '';
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    ENV.host = 'https://mayorsquest.com';
    ENV.namespace = '/api';
  }

  ENV.contentSecurityPolicy = {
    contentSecurityPolicy: {
      'connect-src': "'self' " + ENV.host,
    }
  };

  return ENV;
};
